module.exports = {
    transform : {'^.+\\.ts?$':'ts-jest'},
    preset : 'ts-jest',
    testEnvironment : 'node',
    testTimeout : 60000,
    setupFiles : ['<rootDir>/config/.env.jest.js']
}