INSERT INTO `resources` (`id`, `name`) VALUES (34, 'partner'),
    (35, 'admin'),
    (36, 'resource:create'), (37, 'resource:edit'), (38, 'resource:delete'),
    (39, 'payment-method:create'), (40, 'payment-method:edit'), (41, 'payment-method:delete'), (42, 'payment-method:read'),
    (43, 'service-type:edit'), (44, 'service-type:delete');

INSERT INTO `access_level` (`user_type_id`, `resource_id`) VALUES (1, 35), (1, 36), (1, 37), (1, 38), (1, 39), (1, 40), (1, 41), (1, 42), (1, 43), (1, 44);
INSERT INTO `access_level` (`user_type_id`, `resource_id`) VALUES (2, 34);

ALTER TABLE `services`
    ADD COLUMN `professional_value_acceptance` TINYINT DEFAULT NULL,
    ADD COLUMN `user_value_acceptance` TINYINT DEFAULT NULL;