-- Colocar no /etc/mysql/my.cnf
-- [mysqld-8.0]
-- sql_mode="TRADITIONAL"

CREATE TABLE `user_types`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
)Engine=InnoDB;

CREATE TABLE `users`(
    `name` VARCHAR(50) NOT NULL,
    `surname` VARCHAR(100) NOT NULL,
    `birthdate` DATE NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT now(),
    `user_type_id` INT(11) NOT NULL,
    `last_access` DATETIME NOT NULL DEFAULT now(),
    `email` VARCHAR(150) NOT NULL,
    `password` VARCHAR(150) NOT NULL,
    PRIMARY KEY (`email`),
    FOREIGN KEY (`user_type_id`) REFERENCES `user_types`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `resources`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `type` VARCHAR(50) DEFAULT NULL,
    PRIMARY KEY (`id`)
)Engine=InnoDB;

CREATE TABLE `access_level`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `resource_id` INT(11) NOT NULL,
    `user_type_id` INT(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`resource_id`) REFERENCES `resources`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`user_type_id`) REFERENCES `user_types`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `payment_methods`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
)Engine=InnoDB;

CREATE TABLE `service_types`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `code` VARCHAR(6) NOT NULL,
    `requires_multiple_addresses` tinyint DEFAULT '0',
    PRIMARY KEY (`id`)
)Engine=InnoDB;

CREATE TABLE `offered_service_types`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `service_type_id` INT(11) NOT NULL,
    `professional_email` VARCHAR(150) NOT NULL,
    `base_value` DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`service_type_id`) REFERENCES `service_types`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (`professional_email`) REFERENCES `users`(`email`) ON DELETE CASCADE ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `accepted_payment_methods`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `payment_method_id` INT(11) NOT NULL,
    `professional_email` VARCHAR(150) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (`professional_email`) REFERENCES `users`(`email`) ON DELETE CASCADE ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `status`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `short` VARCHAR(2) NOT NULL,
    `is_ending` TINYINT(1) DEFAULT 0,
    PRIMARY KEY (`id`)
)Engine=InnoDB;

CREATE TABLE `services`(
    `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
    `service_type_id` INT(11) NOT NULL,
    `professional_email` VARCHAR(150),
    `user_email` VARCHAR(150),
    `value` DECIMAL(10,2) NOT NULL,
    `description` VARCHAR(500) NOT NULL,
    `created_at` DATETIME DEFAULT now(),
    `modified_at` DATETIME DEFAULT now(),
    `status_id` INT(11) NOT NULL,
    `payment_method_id` INT(11) NOT NULL,
    `service_date` DATETIME DEFAULT NULL,
    `finished_at` DATETIME DEFAULT NULL,
    `base_value` DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`service_type_id`) REFERENCES `service_types`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (`user_email`) REFERENCES `users`(`email`) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (`professional_email`) REFERENCES `users`(`email`) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (`status_id`) REFERENCES `status`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `reviews`(
    `rate` INT NOT NULL,
    `comment` VARCHAR(240),
    `user_email` VARCHAR(150),
    `created_at` DATETIME NOT NULL DEFAULT now(),
    `service_id` BIGINT(11) NOT NULL,
    PRIMARY KEY (`service_id`),
    FOREIGN KEY (`service_id`) REFERENCES `services`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`user_email`) REFERENCES `users`(`email`) ON DELETE SET NULL ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `messages`(
    `id` INT(11) not null AUTO_INCREMENT,
    `from` VARCHAR(150),
    `message` VARCHAR(240) NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT now(),
    `service_id` BIGINT(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`from`) REFERENCES `users`(`email`) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (`service_id`) REFERENCES `services`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `addresses`(
	`id` INT AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    `address` VARCHAR(255) NOT NULL,
    `city` VARCHAR(100) NOT NULL,
    `state` VARCHAR(50) NOT NULL,
    `country` VARCHAR(50) NOT NULL,
    `zipcode` VARCHAR(10) NOT NULL,
    `number` INT NOT NULL,
    `district` VARCHAR(50) NOT NULL,
    `user_email` VARCHAR(150) NOT NULL,
    `complement` VARCHAR(150) DEFAULT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY(`user_email`) REFERENCES `users`(`email`) ON DELETE CASCADE ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `service_addresses`(
    `id` INT AUTO_INCREMENT,
    `service_id` BIGINT(11) NOT NULL,
    `address_id` INT DEFAULT NULL,
    `name` VARCHAR(100) NOT NULL,
    `address` VARCHAR(255) NOT NULL,
    `city` VARCHAR(100) NOT NULL,
    `state` VARCHAR(50) NOT NULL,
    `country` VARCHAR(50) NOT NULL,
    `zipcode` VARCHAR(10) NOT NULL,
    `number` INT NOT NULL,
    `district` VARCHAR(50) NOT NULL,
    `complement` VARCHAR(150) DEFAULT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY (`address_id`) REFERENCES `addresses`(`id`)ON DELETE CASCADE ON UPDATE SET NULL,
    FOREIGN KEY (`service_id`) REFERENCES `services`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
)Engine=InnoDB;

CREATE TABLE `service_versions`(
    `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
    `payment_method_id` INT(11) NOT NULL,
    `value` DECIMAL(10,2) NOT NULL,
    `description` VARCHAR(500) NOT NULL,
    `created_at` DATETIME DEFAULT now(),
    `modified_at` DATETIME DEFAULT now(),
    `service_date` DATETIME DEFAULT NULL,
    `service_id` BIGINT(11) NOT NULL,
    `made_by` VARCHAR(150) default null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`service_id`) REFERENCES `services`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods`(`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (`made_by`) REFERENCES `users`(`email`) ON DELETE SET NULL ON UPDATE CASCADE
)Engine=InnoDB;

CREATE INDEX `last_access_idx` ON `users`(`last_access`);
CREATE INDEX `name_idx` ON `resources`(`name`);
CREATE UNIQUE INDEX `user_type_id_resource_id_idx` ON `access_level`(`user_type_id`, `resource_id`);
CREATE UNIQUE INDEX `professional_email_service_type_id_idx` ON `offered_service_types`(`professional_email`, `service_type_id`);
CREATE UNIQUE INDEX `professional_email_payment_method_id_idx` ON `accepted_payment_methods`(`professional_email`, `payment_method_id`);
CREATE UNIQUE INDEX `service_id_address_id_idx` ON `service_addresses` (`service_id`, `address_id`);
CREATE INDEX `created_at_idx` ON `services`(`created_at`);
CREATE INDEX `created_at_idx` ON `reviews`(`created_at`);
CREATE INDEX `created_at_idx` ON `messages`(`created_at`);

INSERT INTO `user_types` VALUES (1, 'Administrador'),(2, 'Parceiro'),(3, 'Consumidor');
INSERT INTO `users` (`name`, `surname`, `birthdate`, `user_type_id`, `email`, `password`) VALUES ('Silas', 'Ferreira Lopes', '1994-07-18', 1, 'silasferreiralopes@hotmail.com', 'c25778ce412969d2026ac2bbb8dba260731a31433717f2e4ddd80ac6f15362b1')
INSERT INTO `payment_methods` (`name`) VALUES ('Cartão de débito'), ('Cartão de crédito'), ('Dinheiro'), ('Boleto'), ('Pix');
INSERT INTO `status` (`id`, `name`, `short`, `is_ending`) VALUES(1, 'Aberto', 'A', 0),(2, 'Cancelado', 'C', 1), (3, 'Encerrado', 'E', 1),(4, 'Aguardando Finalização do Serviço', 'AF', 0);
INSERT INTO `resources` (`id`, `name`) VALUES 
    (1, 'accepted-payment:create'), (2, 'accepted-payment:delete'), (3, 'accepted-payment:read'),
    (4, 'access:grant'), (5, 'access:revoke'), (6, 'access:read'), 
    (7, 'address:create'), (8, 'address:edit'), (9, 'address:delete'), (10, 'address:read'),
    (11, 'message:create'), (12, 'message:read'),
    (13, 'offered-service:create'), (14, 'offered-service:delete'), (15, 'offered-service:read'), 
    (16, 'payment-method:read'), 
    (17, 'resource:read'), 
    (18, 'reviews:create'), (19, 'review:edit'), (20, 'review:delete'), (21, 'review:read'),
    (22, 'service:create'), (23, 'service:edit'), (24, 'service:delete'), (25, 'service:read'), 
    (26, 'service-type:create'), (27, 'service-type:read'), 
    (28, 'status:read'), 
    (29, 'user:edit'), (30, 'user:delete'), (31, 'user:read'), 
    (32, 'user-type:read'), 
    (33, 'partner:services');

-- Permissões do Administrador
INSERT INTO `access_level` (`resource_id`, `user_type_id`) VALUES (3, 1), (4, 1), (5, 1), (6, 1), (7, 1), (8, 1), (9, 1), (10, 1), (11, 1), (12, 1), (15, 1), (16, 1), (17, 1), (18, 1), (19, 1), (20, 1), (21, 1), (22, 1), (23, 1), (24, 1), (25, 1), (26, 1), (27, 1), (28, 1), (29, 1), (30, 1), (31, 1), (32, 1);
-- Permissões do Parceiro
INSERT INTO `access_level` (`resource_id`, `user_type_id`) VALUES (1, 2), (2, 2), (3, 2), (7, 2), (8, 2), (9, 2), (10, 2), (11, 2), (12, 2), (13, 2), (14, 2), (15, 2), (16, 2), (18, 2), (19, 2), (20, 2), (21, 2), (22, 2), (23, 2), (24, 2), (25, 2), (26, 2), (27, 2), (28, 2), (29, 2), (30, 2), (31, 2), (32, 2), (33, 2);
-- Permissões do Consumidor
INSERT INTO `access_level` (`resource_id`, `user_type_id`) VALUES (3, 3), (7, 3), (8, 3), (9, 3), (10, 3), (11, 3), (12, 3), (15, 3), (16, 3), (18, 3), (19, 3), (20, 3), (21, 3), (22, 3), (23, 3), (24, 3), (25, 3), (27, 3), (28, 3), (29, 3), (30, 3), (31, 3), (32, 3);
-- dba pass JYi9ob*^87DCPFKJmhKy