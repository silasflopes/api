import ResourcesController from '../src/controllers/ResourcesController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not each here';
describe('Testing ResourcesController.validateName', () => {
  test('Name is ok', () => {
    try {
      expect(ResourcesController.validateName({ name: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Name is empty', () => {
    try {
      ResourcesController.validateName({ name: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Name is not a string', () => {
    try {
      // @ts-ignore
      ResourcesController.validateName({ name: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
