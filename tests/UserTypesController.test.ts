import InvalidParameterValueException from '../src/response-types/4XX/InvalidParameterValueException';
import UserTypesController from '../src/controllers/UserTypesController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';

const reachError = 'Should not each here';
describe('Testing UserTypesController.validateResourcesIds', () => {
  test('resources_ids is OK', () => {
    try {
      expect(UserTypesController.validateResourcesIds({ resources_ids: [1] })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('resources_ids has is not number only', () => {
    try {
      // @ts-ignore
      UserTypesController.validateResourcesIds({ resources_ids: [1, 'abcd'] });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Email is not an array', () => {
    try {
      // @ts-ignore
      UserTypesController.validateResourcesIds({ resources_ids: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
