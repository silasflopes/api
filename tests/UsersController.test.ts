import InvalidParameterValueException from '../src/response-types/4XX/InvalidParameterValueException';
import UsersController from '../src/controllers/UsersController';
import moment from 'moment-timezone';
import MissingParameterException from '../src/response-types/4XX/MissingParameterException';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not each here';
describe('Testing UsersController.validateUserEmail', () => {
  test('Valid email', () => {
    try {
      expect(UsersController.validateUserEmail({ email: 'silasferreiralopes@gmail.com' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Invalid email', () => {
    try {
      UsersController.validateUserEmail({ email: 'abcd' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Email is not a string', () => {
    try {
      // @ts-ignore
      UsersController.validateUserEmail({ email: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing UsersController.validateName', () => {
  test('Name is ok', () => {
    try {
      expect(UsersController.validateName({ name: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Name is empty', () => {
    try {
      UsersController.validateName({ name: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Name is not a string', () => {
    try {
      // @ts-ignore
      UsersController.validateName({ name: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing UsersController.validateSurname', () => {
  test('Surname is ok', () => {
    try {
      expect(UsersController.validateSurname({ surname: 'Ferreira Lopes' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Surname is empty', () => {
    try {
      UsersController.validateSurname({ surname: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Surname is not a string', () => {
    try {
      // @ts-ignore
      UsersController.validateSurname({ surname: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing UsersController.validateBirthdate', () => {
  test('Birthdate is ok', () => {
    try {
      expect(UsersController.validateBirthdate({ birthdate: '1994-07-18' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Birthdate is empty', () => {
    try {
      UsersController.validateBirthdate({ birthdate: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('User is underage', () => {
    try {
      UsersController.validateBirthdate({ birthdate: moment().tz('America/Sao_Paulo').format('YYYY-MM-DD') });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Birthdate is not a string', () => {
    try {
      // @ts-ignore
      UsersController.validateBirthdate({ birthdate: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('Birthdate is not valid', () => {
    try {
      // @ts-ignore
      UsersController.validateBirthdate({ birthdate: '123' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
});
describe('Testing UsersController.validatePassword', () => {
  test('Password is ok', () => {
    try {
      expect(UsersController.validatePassword({ password: 'P!l34@$wAoGe', confirm_password: 'P!l34@$wAoGe' })).toBe(
        true
      );
    } catch (error) {
      fail(reachError);
    }
  });
  test('Password is empty', () => {
    try {
      UsersController.validatePassword({ password: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Password is weak', () => {
    try {
      UsersController.validatePassword({ password: '1234' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Password is not a string', () => {
    try {
      // @ts-ignore
      UsersController.validatePassword({ password: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing UsersController.validateUserTypeId', () => {
  test('user_type_id is ok', () => {
    try {
      expect(UsersController.validateUserTypeId({ user_type_id: 1 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('user_type_id is not a number', () => {
    try {
      // @ts-ignore
      UsersController.validateUserTypeId({ user_type_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing UsersController.validateFields', () => {
  test('Name is missing', () => {
    try {
      UsersController.validateFields({
        surname: '',
        email: '',
        birthdate: '',
        password: '',
        user_type_id: 1
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Surname is missing', () => {
    try {
      UsersController.validateFields({
        name: '',
        email: '',
        birthdate: '',
        password: '',
        user_type_id: 1
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Email is missing', () => {
    try {
      UsersController.validateFields({
        name: '',
        surname: '',
        birthdate: '',
        password: '',
        user_type_id: 1
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Birthdate is missing', () => {
    try {
      UsersController.validateFields({
        name: '',
        surname: '',
        email: '',
        password: '',
        user_type_id: 1
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Password is missing', () => {
    try {
      UsersController.validateFields({
        name: '',
        surname: '',
        email: '',
        birthdate: '',
        user_type_id: 1
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('user_type_id is missing', () => {
    try {
      UsersController.validateFields({
        name: '',
        surname: '',
        email: '',
        birthdate: '',
        password: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
});
