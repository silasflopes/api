import ServicesController from '../src/controllers/ServicesController';
import MissingParameterException from '../src/response-types/4XX/MissingParameterException';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import moment from 'moment-timezone';
import MissingValueException from '../src/response-types/4XX/MissingValueException';
import InvalidParameterValueException from '../src/response-types/4XX/InvalidParameterValueException';
import Users from '../src/database/models/Users';

const reachError = 'Should not each here';

describe('Testing ServicesController.validatePaymentMethodId', () => {
  test('payment_method_id is ok', () => {
    try {
      expect(ServicesController.validatePaymentMethodId({ payment_method_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('payment_method_id is not a number', () => {
    try {
      // @ts-ignore
      ServicesController.validatePaymentMethodId({ payment_method_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateServiceTypeId', () => {
  test('service_type_id is ok', () => {
    try {
      expect(ServicesController.validateServiceTypeId({ service_type_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('service_type_id is not a number', () => {
    try {
      // @ts-ignore
      ServicesController.validateServiceTypeId({ service_type_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateServiceDate', () => {
  test('service_date is ok', () => {
    try {
      expect(
        ServicesController.validateServiceDate({
          service_date: moment().add(1, 'day').tz('America/Sao_Paulo').format('YYYY-MM-DD')
        })
      ).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('service_date is not a string', () => {
    try {
      // @ts-ignore
      ServicesController.validateServiceDate({ service_date: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('service_date is not valid', () => {
    try {
      // @ts-ignore
      ServicesController.validateServiceDate({ service_date: '123' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('service_date is in past', () => {
    try {
      // @ts-ignore
      ServicesController.validateServiceDate({
        service_date: moment().tz('America/Sao_Paulo').subtract(1, 'day').format('YYYY-MM-DD')
      });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
});
describe('Testing ServicesController.validateDescription', () => {
  test('Description is ok', () => {
    try {
      expect(ServicesController.validateDescription({ description: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Description is empty', () => {
    try {
      ServicesController.validateDescription({ description: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Description is not a string', () => {
    try {
      // @ts-ignore
      ServicesController.validateDescription({ description: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateBaseValue', () => {
  test('Base Value is ok', () => {
    try {
      expect(ServicesController.validateBaseValue({ base_value: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Base Value is not a number', () => {
    try {
      // @ts-ignore
      ServicesController.validateBaseValue({ base_value: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateAddresses', () => {
  test('Addresses is ok', () => {
    try {
      expect(new ServicesController(new Users()).validateAddresses({ addresses: [] })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Addresses is not an array', () => {
    try {
      // @ts-ignore
      new ServicesController().validateAddresses({ addresses: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateValue', () => {
  test('value is ok', () => {
    try {
      expect(ServicesController.validateValue({ value: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('value is not a number', () => {
    try {
      // @ts-ignore
      ServicesController.validateValue({ value: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateProfessionalAcceptance', () => {
  test('professional_acceptance is ok', () => {
    try {
      expect(ServicesController.validateProfessionalAcceptance({ professional_acceptance: false })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('professional_acceptance is not a number', () => {
    try {
      // @ts-ignore
      ServicesController.validateProfessionalAcceptance({ professional_acceptance: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateUserAcceptance', () => {
  test('user_acceptance is ok', () => {
    try {
      expect(ServicesController.validateUserAcceptance({ user_acceptance: false })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('user_acceptance is not a number', () => {
    try {
      // @ts-ignore
      ServicesController.validateUserAcceptance({ user_acceptance: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServicesController.validateFields', () => {
  test('service_type_id is missing', () => {
    try {
      ServicesController.validateFields({
        payment_method_id: 1000,
        user_email: 'string@email.com',
        professional_email: 'string@email.com',
        description: 'string',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD'),
        base_value: 522,
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('payment_method_id is missing', () => {
    try {
      ServicesController.validateFields({
        service_type_id: 2000,
        user_email: 'string@email.com',
        professional_email: 'string@email.com',
        description: 'string',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD'),
        base_value: 522,
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('user_email is missing', () => {
    try {
      ServicesController.validateFields({
        service_type_id: 2000,
        payment_method_id: 1000,
        professional_email: 'string@email.com',
        description: 'string',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD'),
        base_value: 522,
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('professional_email is missing', () => {
    try {
      ServicesController.validateFields({
        service_type_id: 2000,
        payment_method_id: 1000,
        user_email: 'string@email.com',
        description: 'string',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD'),
        base_value: 522,
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('description is missing', () => {
    try {
      ServicesController.validateFields({
        service_type_id: 2000,
        payment_method_id: 1000,
        user_email: 'string@email.com',
        professional_email: 'string@email.com',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD'),
        base_value: 522,
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('service_date is missing', () => {
    try {
      ServicesController.validateFields({
        service_type_id: 2000,
        payment_method_id: 1000,
        user_email: 'string@email.com',
        professional_email: 'string@email.com',
        description: 'string',
        base_value: 522,
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('base_value is missing', () => {
    try {
      ServicesController.validateFields({
        service_type_id: 2000,
        payment_method_id: 1000,
        user_email: 'string@email.com',
        professional_email: 'string@email.com',
        description: 'string',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD'),
        addresses: []
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('addresses is missing', () => {
    try {
      // @ts-ignore
      ServicesController.validateFields({
        service_type_id: 2000,
        payment_method_id: 1000,
        user_email: 'string@email.com',
        professional_email: 'string@email.com',
        description: 'string',
        service_date: moment().tz('America/Sao_Paulo').add(1, 'day').format('YYYY-MM-DD')
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
});
