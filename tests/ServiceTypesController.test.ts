import InvalidParameterValueException from '../src/response-types/4XX/InvalidParameterValueException';
import ServiceTypesController from '../src/controllers/ServiceTypesController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not each here';
describe('Testing ServiceTypesController.validateName', () => {
  test('Name is ok', () => {
    try {
      expect(ServiceTypesController.validateName({ name: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Name is empty', () => {
    try {
      ServiceTypesController.validateName({ name: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Name is not a string', () => {
    try {
      // @ts-ignore
      ServiceTypesController.validateName({ name: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServiceTypesController.generateCode', () => {
  test('Name has less than 3 letters', async () => {
    try {
      await ServiceTypesController.generateCode('aa');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Should get first 3 letter from the word', async () => {
    try {
      ServiceTypesController.getCodeSufix = jest.fn();
      // @ts-ignore
      ServiceTypesController.getCodeSufix.mockReturnValueOnce('01');
      const code = await ServiceTypesController.generateCode('abcde');
      expect(code).toBe('ABC01');
    } catch (error) {
      fail(reachError);
    }
  });
  test('Should get first 3 letter from first word', async () => {
    try {
      ServiceTypesController.getCodeSufix = jest.fn();
      // @ts-ignore
      ServiceTypesController.getCodeSufix.mockReturnValueOnce('01');
      const code = await ServiceTypesController.generateCode('abcde ddef');
      expect(code).toBe('ABC01');
    } catch (error) {
      fail(reachError);
    }
  });
  test('Should get first letter from the 3 first words', async () => {
    try {
      ServiceTypesController.getCodeSufix = jest.fn();
      // @ts-ignore
      ServiceTypesController.getCodeSufix.mockReturnValueOnce('01');
      const code = await ServiceTypesController.generateCode('Abcde fghi Jlmn');
      expect(code).toBe('AFJ01');
    } catch (error) {
      fail(reachError);
    }
  });
  test('Should ignore words with less then 3 letters', async () => {
    try {
      ServiceTypesController.getCodeSufix = jest.fn();
      // @ts-ignore
      ServiceTypesController.getCodeSufix.mockReturnValueOnce('01');
      const code = await ServiceTypesController.generateCode('Abcde aa fghi Jlmn');
      expect(code).toBe('AFJ01');
    } catch (error) {
      fail(reachError);
    }
  });
  test('Should increment appended number', async () => {
    try {
      ServiceTypesController.getCodeSufix = jest.fn();
      // @ts-ignore
      ServiceTypesController.getCodeSufix.mockReturnValueOnce('02');
      const code = await ServiceTypesController.generateCode('Abcde aa fghi Jlmn');
      expect(code).toBe('AFJ02');
    } catch (error) {
      fail(reachError);
    }
  });
});
