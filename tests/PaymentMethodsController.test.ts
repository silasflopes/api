import PaymentMethodsController from '../src/controllers/PaymentMethodsController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not each here';
describe('Testing PaymentMethodsController.validateName', () => {
  test('Name is ok', () => {
    try {
      expect(PaymentMethodsController.validateName({ name: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Name is empty', () => {
    try {
      PaymentMethodsController.validateName({ name: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Name is not a string', () => {
    try {
      // @ts-ignore
      PaymentMethodsController.validateName({ name: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
