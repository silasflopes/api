import AcceptedPaymentMethodsController from '../src/controllers/AcceptedPaymentMethodsController';
import MissingParameterException from '../src/response-types/4XX/MissingParameterException';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';

const reachError = 'Should not each here';

describe('Testing AcceptedPaymentMethodsController.validatePaymentMethodId', () => {
  test('payment_method_id is ok', () => {
    try {
      expect(AcceptedPaymentMethodsController.validatePaymentMethodId({ payment_method_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('payment_method_id is not a number', () => {
    try {
      // @ts-ignore
      AcceptedPaymentMethodsController.validatePaymentMethodId({ payment_method_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AcceptedPaymentMethodsController.validateFields', () => {
  test('payment_method_id is missing', () => {
    try {
      AcceptedPaymentMethodsController.validateFields({
        professional_email: 'email@email.com'
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('professional_email is missing', () => {
    try {
      AcceptedPaymentMethodsController.validateFields({
        payment_method_id: 10
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
});
