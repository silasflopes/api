import AddressesController from '../src/controllers/AddressesController';
import MissingParameterException from '../src/response-types/4XX/MissingParameterException';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not each here';
describe('Testing AddressesController.validateName', () => {
  test('Name is ok', () => {
    try {
      expect(AddressesController.validateName({ name: 'Casa' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Name is empty', () => {
    try {
      AddressesController.validateName({ name: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Name is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateName({ name: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateAddress', () => {
  test('Address is ok', () => {
    try {
      expect(AddressesController.validateAddress({ address: 'Rua Abc de 123' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Address is empty', () => {
    try {
      AddressesController.validateAddress({ address: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Address is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateAddress({ address: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateHouseNumber', () => {
  test('Number is ok', () => {
    try {
      expect(AddressesController.validateHouseNumber({ number: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Number is not a number', () => {
    try {
      // @ts-ignore
      AddressesController.validateHouseNumber({ number: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateComplement', () => {
  test('Complement is ok', () => {
    try {
      expect(AddressesController.validateComplement({ complement: 'Apto 556' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Complement is empty', () => {
    try {
      AddressesController.validateComplement({ complement: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Complement is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateComplement({ complement: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateDistrict', () => {
  test('District is ok', () => {
    try {
      expect(AddressesController.validateDistrict({ district: 'Centro' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('District is empty', () => {
    try {
      AddressesController.validateDistrict({ district: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('District is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateDistrict({ district: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateCity', () => {
  test('City is ok', () => {
    try {
      expect(AddressesController.validateCity({ city: 'Belo Horizante' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('City is empty', () => {
    try {
      AddressesController.validateCity({ city: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('City is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateCity({ city: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateState', () => {
  test('State is ok', () => {
    try {
      expect(AddressesController.validateState({ state: 'MG' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('State is empty', () => {
    try {
      AddressesController.validateState({ state: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('State is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateState({ state: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateCountry', () => {
  test('Country is ok', () => {
    try {
      expect(AddressesController.validateCountry({ country: 'Brasil' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Country is empty', () => {
    try {
      AddressesController.validateCountry({ country: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Country is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateCountry({ country: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateZipcode', () => {
  test('Zipcode is ok', () => {
    try {
      expect(AddressesController.validateZipcode({ zipcode: '22556698' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Zipcode is empty', () => {
    try {
      AddressesController.validateZipcode({ zipcode: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('Zipcode is not a string', () => {
    try {
      // @ts-ignore
      AddressesController.validateZipcode({ zipcode: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AddressesController.validateFields', () => {
  test('Name is missing', () => {
    try {
      AddressesController.validateFields({
        address: '',
        number: 123,
        complement: '',
        district: '',
        city: '',
        state: '',
        country: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Address is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        number: 123,
        complement: '',
        district: '',
        city: '',
        state: '',
        country: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Number is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        complement: '',
        district: '',
        city: '',
        state: '',
        country: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('District is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        number: 123,
        complement: '',
        city: '',
        state: '',
        country: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('City is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        number: 123,
        complement: '',
        district: '',
        state: '',
        country: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('State is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        number: 123,
        complement: '',
        district: '',
        city: '',
        country: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Country is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        number: 123,
        complement: '',
        district: '',
        city: '',
        state: '',
        zipcode: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Zipcode is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        number: 123,
        complement: '',
        district: '',
        city: '',
        state: '',
        country: ''
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('Complement is missing', () => {
    try {
      AddressesController.validateFields({
        name: '',
        address: '',
        number: 123,
        district: '',
        city: '',
        state: '',
        country: '',
        zipcode: ''
      });
      expect(1);
    } catch (error) {
      fail(reachError);
    }
  });
});
