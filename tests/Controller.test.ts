import Controller from '../src/controllers/Controller';
import InvalidParameterValueException from '../src/response-types/4XX/InvalidParameterValueException';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not reach here';
describe('Testing Controller.validateObject', () => {
  test('body as object', () => {
    try {
      expect(Controller.validateObject({})).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('body as string', () => {
    try {
      Controller.validateObject('value');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('body as string with fieldName', () => {
    try {
      Controller.validateObject('value', 'campo');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('body as number', () => {
    try {
      Controller.validateObject(123);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('body as boolean', () => {
    try {
      Controller.validateObject(false);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('body as array', () => {
    try {
      Controller.validateObject([]);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('body as null', () => {
    try {
      Controller.validateObject(null);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('body as undefined', () => {
    try {
      Controller.validateObject(undefined);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing Controller.validateString', () => {
  test('variable as string', () => {
    try {
      expect(Controller.validateString('value')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('variable as object', () => {
    try {
      // @ts-ignore
      Controller.validateString({});
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('variable as object with fieldName', () => {
    try {
      // @ts-ignore
      Controller.validateString({}, 'campo');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('variable as number', () => {
    try {
      // @ts-ignore
      Controller.validateString(123);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('variable as boolean', () => {
    try {
      // @ts-ignore
      Controller.validateString(false);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('variable as array', () => {
    try {
      // @ts-ignore
      Controller.validateString([]);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('variable as null', () => {
    try {
      // @ts-ignore
      Controller.validateString(null);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('variable as undefined', () => {
    try {
      // @ts-ignore
      Controller.validateString(undefined);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing Controller.validateArray', () => {
  test('array as array', () => {
    try {
      expect(Controller.validateArray([])).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('array as string', () => {
    try {
      // @ts-ignore
      Controller.validateArray('value');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('array as string with fieldName', () => {
    try {
      // @ts-ignore
      Controller.validateArray('value', 'campo');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('array as number', () => {
    try {
      // @ts-ignore
      Controller.validateArray(123);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('array as boolean', () => {
    try {
      // @ts-ignore
      Controller.validateArray(false);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('array as object', () => {
    try {
      // @ts-ignore
      Controller.validateArray({});
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('array as null', () => {
    try {
      // @ts-ignore
      Controller.validateArray(null);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('array as undefined', () => {
    try {
      // @ts-ignore
      Controller.validateArray(undefined);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing Controller.validateNumber', () => {
  test('num as number', () => {
    try {
      expect(Controller.validateNumber(123)).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('num as string', () => {
    try {
      // @ts-ignore
      Controller.validateNumber('value');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('num as string with fieldName', () => {
    try {
      // @ts-ignore
      Controller.validateNumber('value', 'campo');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('num as array', () => {
    try {
      // @ts-ignore
      Controller.validateNumber([]);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('num as boolean', () => {
    try {
      // @ts-ignore
      Controller.validateNumber(false);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('num as object', () => {
    try {
      // @ts-ignore
      Controller.validateNumber({});
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('num as null', () => {
    try {
      // @ts-ignore
      Controller.validateNumber(null);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('num as undefined', () => {
    try {
      // @ts-ignore
      Controller.validateNumber(undefined);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing Controller.validateBoolean', () => {
  test('bool as boolean', () => {
    try {
      expect(Controller.validateBoolean(false)).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('bool as string', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean('value');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('bool as string with fieldName', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean('value', 'campo');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('bool as array', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean([]);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('bool as number', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean(123);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('bool as object', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean({});
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('bool as null', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean(null);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
  test('bool as undefined', () => {
    try {
      // @ts-ignore
      Controller.validateBoolean(undefined);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing Controller.validateEmptyObject', () => {
  test('object is not empty', () => {
    try {
      expect(Controller.validateEmptyObject({ prop: 'value' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('object is empty', () => {
    try {
      Controller.validateEmptyObject({});
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
});
describe('Testing Controller.validateEmptyString', () => {
  test('string is not empty', () => {
    try {
      expect(Controller.validateEmptyString('ABC')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('string is empty', () => {
    try {
      Controller.validateEmptyString('');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
});
describe('Testing Controller.validateEmptyArray', () => {
  test('array is not empty', () => {
    try {
      expect(Controller.validateEmptyArray(['ABC'])).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('array is empty', () => {
    try {
      Controller.validateEmptyArray([]);
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
});
describe('Testing Controller.validateDate', () => {
  test('Valid date', () => {
    try {
      expect(Controller.validateDate('2020-04-12')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Valid date in other format', () => {
    try {
      expect(Controller.validateDate('12/04/2020', 'DD/MM/YYYY')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Invalid date', () => {
    try {
      Controller.validateDate('---sdasd');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Invalid date in other format', () => {
    try {
      Controller.validateDate('2020-04-12', 'DD/MM/YYYY');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
});
describe('Testing Controller.validateDateTime', () => {
  test('Valid date', () => {
    try {
      expect(Controller.validateDateTime('2020-04-12 12:17:33')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Valid date in other format', () => {
    try {
      expect(Controller.validateDateTime('12/04/2020 12:17:33', 'DD/MM/YYYY HH:mm:ss')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Invalid date', () => {
    try {
      Controller.validateDateTime('---sdasd');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('Invalid date in other format', () => {
    try {
      Controller.validateDateTime('2020-04-12 12:17:33', 'DD/MM/YYYY HH:mm:ss');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
});
describe('Testing Controller.validateEmail', () => {
  test('Valid email', () => {
    try {
      expect(Controller.validateEmail('silasferreiralopes@gmail.com')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Invalid email', () => {
    try {
      Controller.validateEmail('abcd');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
});
describe('Testing Controller.validateUrl', () => {
  test('url is valid', () => {
    try {
      expect(Controller.validateUrl('https://google.com')).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('url is not valid', () => {
    try {
      Controller.validateUrl('');
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
});
