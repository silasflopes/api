import ServiceAddressesController from '../src/controllers/ServiceAddressesController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';

const reachError = 'Should not each here';

describe('Testing ServiceAddressesController.validateServiceId', () => {
  test('service_id is ok', () => {
    try {
      expect(ServiceAddressesController.validateServiceId({ service_id: '568' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('service_id is not a number', () => {
    try {
      // @ts-ignore
      ServiceAddressesController.validateServiceId({ service_id: 11123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ServiceAddressesController.validateAddressId', () => {
  test('address_id is ok', () => {
    try {
      expect(ServiceAddressesController.validateAddressId({ address_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('address_id is not a number', () => {
    try {
      // @ts-ignore
      ServiceAddressesController.validateAddressId({ address_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
