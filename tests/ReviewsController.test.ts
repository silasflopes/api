import ReviewsController from '../src/controllers/ReviewsController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';
import InvalidParameterValueException from '../src/response-types/4XX/InvalidParameterValueException';
import MissingParameterException from '../src/response-types/4XX/MissingParameterException';
import MissingValueException from '../src/response-types/4XX/MissingValueException';

const reachError = 'Should not each here';
describe('Testing ReviewsController.validateComment', () => {
  test('comment is ok', () => {
    try {
      expect(ReviewsController.validateComment({ comment: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('comment is not a string', () => {
    try {
      // @ts-ignore
      ReviewsController.validateComment({ comment: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ReviewsController.validateRate', () => {
  test('rate is ok', () => {
    try {
      expect(ReviewsController.validateRate({ rate: 1 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('rate is not valid', () => {
    try {
      ReviewsController.validateRate({ rate: 15 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterValueException);
    }
  });
  test('rate is not a number', () => {
    try {
      // @ts-ignore
      ReviewsController.validateRate({ rate: '123' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ReviewsController.validateServiceId', () => {
  test('service_id is ok', () => {
    try {
      expect(ReviewsController.validateServiceId({ service_id: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('service_id is empty', () => {
    try {
      ReviewsController.validateServiceId({ service_id: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('service_id is not a string', () => {
    try {
      // @ts-ignore
      ReviewsController.validateServiceId({ service_id: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing ReviewsController.validateUserEmail', () => {
  test('user_email is ok', () => {
    try {
      expect(ReviewsController.validateUserEmail({ user_email: 'Silas' })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('user_email is empty', () => {
    try {
      ReviewsController.validateUserEmail({ user_email: '' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(MissingValueException);
    }
  });
  test('user_email is not a string', () => {
    try {
      // @ts-ignore
      ReviewsController.validateUserEmail({ user_email: 123 });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});

describe('Testing ReviewsController.validateFields', () => {
  test('rate is missing', () => {
    try {
      ReviewsController.validateFields({
        service_id: '10',
        user_email: 'email@email.com'
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('service_id is missing', () => {
    try {
      ReviewsController.validateFields({
        rate: 4,
        user_email: 'email@email.com'
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
});
