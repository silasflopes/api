import AccessLevelController from '../src/controllers/AccessLevelController';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';

const reachError = 'Should not each here';
describe('Testing AccessLevelController.validateResourceId', () => {
  test('resource_id is ok', () => {
    try {
      expect(AccessLevelController.validateResourceId({ resource_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('resource_id is not a number', () => {
    try {
      // @ts-ignore
      AccessLevelController.validateResourceId({ resource_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing AccessLevelController.validateUserTypeId', () => {
  test('user_type_id is ok', () => {
    try {
      expect(AccessLevelController.validateUserTypeId({ user_type_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('user_type_id is not a number', () => {
    try {
      // @ts-ignore
      AccessLevelController.validateUserTypeId({ user_type_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
