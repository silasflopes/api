import OfferedServiceTypesController from '../src/controllers/OfferedServiceTypesController';
import MissingParameterException from '../src/response-types/4XX/MissingParameterException';
import InvalidParameterTypeException from '../src/response-types/4XX/InvalidParameterTypeException';

const reachError = 'Should not each here';
describe('Testing OfferedServiceTypesController.validateBaseValue', () => {
  test('Base Value is ok', () => {
    try {
      expect(OfferedServiceTypesController.validateBaseValue({ base_value: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('Base Value is not a number', () => {
    try {
      // @ts-ignore
      OfferedServiceTypesController.validateBaseValue({ base_value: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing OfferedServiceTypesController.validateServiceTypeId', () => {
  test('service_type_id is ok', () => {
    try {
      expect(OfferedServiceTypesController.validateServiceTypeId({ service_type_id: 568 })).toBe(true);
    } catch (error) {
      fail(reachError);
    }
  });
  test('service_type_id is not a number', () => {
    try {
      // @ts-ignore
      OfferedServiceTypesController.validateServiceTypeId({ service_type_id: 'ssss' });
      fail(reachError);
    } catch (error) {
      expect(error).toBeInstanceOf(InvalidParameterTypeException);
    }
  });
});
describe('Testing OfferedServiceTypesController.validateFields', () => {
  test('base_value is missing', () => {
    try {
      OfferedServiceTypesController.validateFields({
        service_type_id: 10,
        professional_email: 'email@email.com'
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('service_type_id is missing', () => {
    try {
      OfferedServiceTypesController.validateFields({
        base_value: 10,
        professional_email: 'email@email.com'
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
  test('professional_email is missing', () => {
    try {
      OfferedServiceTypesController.validateFields({
        base_value: 10,
        service_type_id: 10
      });
    } catch (error) {
      expect(error).toBeInstanceOf(MissingParameterException);
    }
  });
});
