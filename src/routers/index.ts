import { readdirSync } from 'fs';
import { join } from 'path';
import MethodNotAllowedException from '../response-types/4XX/MethodNotAllowedException';
import NotFoundException from '../response-types/4XX/NotFoundException';
import { Express, Request, Response, NextFunction } from 'express';

export function route(app: Express): void {
  app.all('/', async (request: Request, response: Response, next: NextFunction) => {
    response.status(200).send('I am healthy! Thanks for asking! :D');
    next();
  });
  const directoryPath = join(__dirname);
  readdirSync(directoryPath)
    .filter((file) => {
      const fileArr = file.split('.');
      return file.indexOf('.') !== 0 && fileArr[0] !== 'index' && (fileArr[1] === 'ts' || fileArr[1] === 'js');
    })
    .forEach((file) => {
      const { router } = require(`./${file}`);
      router(app);
    });
  app.use((request: Request, response: Response, next: NextFunction) => {
    // Verifica se o header já foi enviado, pois significa que já houve execução do request, e portanto, houve match das rotas da api
    if (response.headersSent) return next();
    // Pega todas as rotas da api
    const routes = app._router.stack.filter((apiRoute: any) => typeof apiRoute.route !== 'undefined');

    const matchedRoutes = routes.filter((apiRoute: any) => typeof apiRoute.path !== 'undefined');

    if (matchedRoutes.length === 0) {
      // Se não teve match, retorna 404
      return next(new NotFoundException(`URL '${request.path}' não encontrada`));
    } else {
      const methods: string[] = [];
      for (const matchedRoute of matchedRoutes) {
        methods.push(...Object.keys(matchedRoute.route.methods));
      }
      if (
        typeof methods.find((method: string) => method.toLowerCase() === request.method.toLowerCase()) === 'undefined'
      ) {
        // Retorna 405, pois não achou o método
        return next(
          new MethodNotAllowedException(
            `Método '${request.method.toUpperCase()}' não permitido na URL '${request.path}'`
          )
        );
      } else {
        // Achou o método, não retorno erro
        return next();
      }
    }
  });
}
