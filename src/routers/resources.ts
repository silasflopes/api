// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import ResourcesController from '../controllers/ResourcesController';
export function router(app: Express): void {
  const path = '/resources';
  app.get(
    `${path}`,
    [Authorizer, AccessControl('resource:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new ResourcesController(request.user).get());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:id`,
    [Authorizer, AccessControl('resource:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new ResourcesController(request.user).getOne(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}`,
    [Authorizer, AccessControl(['admin', 'resource:read', 'resource:create'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(await new ResourcesController(request.user).create(request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.patch(
    `${path}/:id`,
    [Authorizer, AccessControl(['admin', 'resource:read', 'resource:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const resourcesController = new ResourcesController(request.user);
        response.status(200).send(await resourcesController.update(parseInt(request.params.id), request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.delete(
    `${path}/:id`,
    [Authorizer, AccessControl(['admin', 'resource:read', 'resource:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const resourcesController = new ResourcesController(request.user);
        response.status(200).send(await resourcesController.delete(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
