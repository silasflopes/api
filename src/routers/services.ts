// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import ServicesController from '../controllers/ServicesController';
// Exceptions
import NotFoundException from '../response-types/4XX/NotFoundException';
export function router(app: Express): void {
  const path = '/services';

  app.get(
    `${path}/:id`,
    [Authorizer, AccessControl('service:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new ServicesController(request.user).getOne(request.params.id));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `/partners/services`,
    [Authorizer, AccessControl(['partner:services', 'service:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response
          .status(200)
          .send(await new ServicesController(request.user).get({ professional_email: request.user.email }));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
  app.get(
    '/partners/services/:id',
    [Authorizer, AccessControl(['partner:services', 'service:read', 'service:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const service = await new ServicesController(request.user).getOne(request.params.id);
        if (service.professional.email !== request.user.email)
          throw new NotFoundException(`O profissional '${request.user.email}' não possui o serviço ${service.id}`);
        response.status(200).send(service);
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.patch(
    '/partners/services/:id',
    [Authorizer, AccessControl(['partner:services', 'service:read', 'service:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response
          .status(200)
          .send(await new ServicesController(request.user).update(request.params.id, request.body, true));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
