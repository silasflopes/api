// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import ServiceTypesController from '../controllers/ServiceTypesController';
export function router(app: Express): void {
  const path = '/service-types';
  app.get(
    `${path}`,
    [Authorizer, AccessControl('service-type:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new ServiceTypesController(request.user).get());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/users`,
    [
      Authorizer,
      AccessControl(['service-type:read', 'user:read', 'review:read', 'accepted-payment:read', 'offered-service:read'])
    ],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response
          .status(200)
          .send(await new ServiceTypesController(request.user).getProfessionals(request.query.q as string));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:id[0-9]+`,
    [Authorizer, AccessControl('service-type:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new ServiceTypesController(request.user).getOne(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}`,
    [Authorizer, AccessControl(['service-type:read', 'service-type:create'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(await new ServiceTypesController(request.user).create(request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.patch(
    `${path}/:id`,
    [Authorizer, AccessControl(['service-type:read', 'service-type:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const serviceTypesController = new ServiceTypesController(request.user);
        response.status(200).send(await serviceTypesController.update(parseInt(request.params.id), request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.delete(
    `${path}/:id`,
    [Authorizer, AccessControl(['service-type:read', 'service-type:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const serviceTypesController = new ServiceTypesController(request.user);
        response.status(200).send(await serviceTypesController.delete(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
