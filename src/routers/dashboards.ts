// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import DashboardsController from '../controllers/DashboardsController';

export function router(app: Express): void {
  const path = '/dashboards';
  app.get(
    `${path}/monthly-income`,
    [Authorizer, AccessControl('dashboard:monthly-income')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new DashboardsController(request.user).monthlyIncome());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/services-per-month`,
    [Authorizer, AccessControl('dashboard:services-per-month')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new DashboardsController(request.user).servicesPerMonth());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/yearly-income`,
    [Authorizer, AccessControl('dashboard:yearly-income')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new DashboardsController(request.user).yearlyIncome());
      } catch (error: unknown) {
        next(error);
      }
    }
  );
  app.get(
    `${path}/daily-registration`,
    [Authorizer, AccessControl('dashboard:daily-registration')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new DashboardsController(request.user).dailyRegistration());
      } catch (error: unknown) {
        next(error);
      }
    }
  );
  app.get(
    `${path}/payment-methods`,
    [Authorizer, AccessControl('dashboard:payment-methods')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new DashboardsController(request.user).paymentMethods());
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
