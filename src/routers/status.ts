// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import StatusController from '../controllers/StatusController';
export function router(app: Express): void {
  const path = '/status';
  app.get(
    `${path}`,
    [Authorizer, AccessControl('status:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new StatusController(request.user).get());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:id`,
    [Authorizer, AccessControl('status:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new StatusController(request.user).getOne(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
