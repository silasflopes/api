// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import SecurityController from '../controllers/SecurityController';
import AddressesController from '../controllers/AddressesController';
import UsersController from '../controllers/UsersController';
import ServicesController from '../controllers/ServicesController';
import MessagesController from '../controllers/MessagesController';
import ReviewsController from '../controllers/ReviewsController';

export function router(app: Express): void {
  const path = '/users';
  app.get(
    `${path}`,
    [Authorizer, AccessControl('user:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new UsersController(request.user).get({}));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(`${path}/authenticate`, async (request: Request, response: Response, next: NextFunction) => {
    try {
      const [user, token] = await new SecurityController().authenticate(request.headers.authorization as string);
      response.status(200).send({ user, token });
    } catch (error: unknown) {
      next(error);
    }
  });

  app.post(`${path}/facebook-login`, async (request: Request, response: Response, next: NextFunction) => {
    try {
      const [user, token] = await new SecurityController().facebookLogin(request.body);
      response.status(200).send({ user, token });
    } catch (error: unknown) {
      next(error);
    }
  });

  app.get(
    `${path}/:email`,
    [Authorizer, AccessControl('user:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new UsersController(request.user).getOne(request.params.email));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(`${path}`, async (request: Request, response: Response, next: NextFunction) => {
    try {
      response.status(201).send(await new UsersController(request.user).create(request.body));
    } catch (error: unknown) {
      next(error);
    }
  });

  app.patch(
    `${path}/:email`,
    [Authorizer, AccessControl(['user:read', 'user:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.update(request.params.email, request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.delete(
    `${path}/:email`,
    [Authorizer, AccessControl(['user:read', 'user:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.delete(request.params.email));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/addresses`,
    [Authorizer, AccessControl(['user:read', 'address:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.getAddresses(request.params.email));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/addresses/:id`,
    [Authorizer, AccessControl(['user:read', 'address:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.getAddress(request.params.email, parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.patch(
    `${path}/:email/addresses/:id`,
    [Authorizer, AccessControl(['user:read', 'address:read', 'address:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response
          .status(200)
          .send(await usersController.updateAddress(request.params.email, parseInt(request.params.id), request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.delete(
    `${path}/:email/addresses/:id`,
    [Authorizer, AccessControl(['user:read', 'address:read', 'address:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response
          .status(200)
          .send(await usersController.deleteAddress(request.params.email, parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}/:email/addresses`,
    [Authorizer, AccessControl(['user:read', 'address:read', 'address:create'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(
          await new AddressesController(request.user).create({
            ...request.body,
            user_email: request.user.email
          })
        );
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/services`,
    [Authorizer, AccessControl(['user:read', 'service:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.getServices(request.params.email));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/services/:id([0-9]+)`,
    [Authorizer, AccessControl(['user:read', 'service:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.getService(request.params.email, request.params.id));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.patch(
    `${path}/:email/services/:id`,
    [
      Authorizer,
      AccessControl([
        'user:read',
        'service:read',
        'service:edit',
        'status:read',
        'offered-service:read',
        'service-type:read',
        'payment-method:read',
        'accepted-payment:read'
      ])
    ],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response
          .status(200)
          .send(await usersController.updateService(request.params.email, request.params.id, request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.delete(
    `${path}/:email/services/:id`,
    [Authorizer, AccessControl(['user:read', 'service:read', 'service:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.deleteService(request.params.email, request.params.id));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}/:email/services`,
    [
      Authorizer,
      AccessControl([
        'user:read',
        'service:read',
        'service:create',
        'status:read',
        'offered-service:read',
        'service-type:read',
        'payment-method:read',
        'accepted-payment:read'
      ])
    ],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(
          await new ServicesController(request.user).create({
            ...request.body,
            user_email: request.user?.email
          })
        );
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/services/:id/messages`,
    [Authorizer, AccessControl(['user:read', 'service:read', 'message:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        const res = await usersController.getServiceMessages(request.params.email, request.params.id);
        response.status(200).send(res);
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}/:email/services/:id([0-9]+)/messages`,
    [Authorizer, AccessControl(['user:read', 'service:read', 'message:read', 'message:create'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(
          await new MessagesController(request.user).create({
            ...request.body,
            service_id: request.params.id,
            from: request.user?.email
          })
        );
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}/:email/services/:id([0-9]+)/reviews`,
    [Authorizer, AccessControl(['user:read', 'service:read', 'review:read', 'review:create'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(
          await new ReviewsController(request.user).create({
            ...request.body,
            service_id: request.params.id,
            user_email: request.user.email
          })
        );
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.put(
    `${path}/:email/service-types`,
    [Authorizer, AccessControl(['user:read', 'service-type:read', 'offered-service:create', 'offered-service:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.setOfferedServices(request.params.email, request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/service-types`,
    [Authorizer, AccessControl(['user:read', 'service-type:read', 'offered-service:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.getOfferedServices(request.params.email));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.put(
    `${path}/:email/payment-methods`,
    [
      Authorizer,
      AccessControl(['user:read', 'payment-method:read', 'accepted-payment:create', 'accepted-payment:delete'])
    ],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.setPaymentMethods(request.params.email, request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:email/payment-methods`,
    [Authorizer, AccessControl(['user:read', 'payment-method:read', 'accepted-payment:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const usersController = new UsersController(request.user);
        response.status(200).send(await usersController.getPaymentMethods(request.params.email));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
