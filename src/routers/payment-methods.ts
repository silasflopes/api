// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import PaymentMethodsController from '../controllers/PaymentMethodsController';

export function router(app: Express): void {
  const path = '/payment-methods';
  app.get(
    `${path}`,
    [Authorizer, AccessControl('payment-method:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new PaymentMethodsController(request.user).get());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:id`,
    [Authorizer, AccessControl('payment-method:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new PaymentMethodsController(request.user).getOne(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}`,
    [Authorizer, AccessControl(['admin', 'payment-method:read', 'payment-method:create'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(201).send(await new PaymentMethodsController(request.user).create(request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.patch(
    `${path}/:id`,
    [Authorizer, AccessControl(['admin', 'payment-method:read', 'payment-method:edit'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const paymentMethodsController = new PaymentMethodsController(request.user);
        response.status(200).send(await paymentMethodsController.update(parseInt(request.params.id), request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.delete(
    `${path}/:id`,
    [Authorizer, AccessControl(['admin', 'payment-method:read', 'payment-method:delete'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        const paymentMethodsController = new PaymentMethodsController(request.user);
        response.status(200).send(await paymentMethodsController.delete(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
