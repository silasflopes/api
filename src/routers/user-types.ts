// Libs
import { Express, Request, Response, NextFunction } from 'express';
// Middlewares
import Authorizer from '../middlewares/Authorizer';
import AccessControl from '../middlewares/AccessControl';
// Controllers
import UserTypesController from '../controllers/UserTypesController';
// Response types
import OperationNotPermittedException from '../response-types/4XX/OperationNotPermittedException';

export function router(app: Express): void {
  const path = '/user-types';
  app.get(`${path}`, async (request: Request, response: Response, next: NextFunction) => {
    try {
      response.status(200).send(await new UserTypesController(request.user).get());
    } catch (error: unknown) {
      next(error);
    }
  });

  app.get(
    `${path}/permissions`,
    [Authorizer, AccessControl(['user-type:read', 'access:read', 'resource:read'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new UserTypesController(request.user).getPermissions());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:id([0-9]+)`,
    [Authorizer, AccessControl('user-type:read')],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response.status(200).send(await new UserTypesController(request.user).getOne(parseInt(request.params.id)));
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.get(
    `${path}/:id([0-9]+)/permissions`,
    [Authorizer],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        if (request.user.userType.id !== parseInt(request.params.id))
          throw new OperationNotPermittedException('Operação não permitida');
        response.status(200).send(await new UserTypesController(request.user).getUserTypePermissions());
      } catch (error: unknown) {
        next(error);
      }
    }
  );

  app.post(
    `${path}/:id([0-9]+)/permissions`,
    [Authorizer, AccessControl(['user-type:read', 'resource:read', 'access:grant'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response
          .status(200)
          .send(await new UserTypesController(request.user).addPermissions(parseInt(request.params.id), request.body));
      } catch (error: unknown) {
        next(error);
      }
    }
  );
  app.delete(
    `${path}/:id([0-9]+)/permissions/:resourceId([0-9]+)`,
    [Authorizer, AccessControl(['user-type:read', 'resource:read', 'access:revoke'])],
    async (request: Request, response: Response, next: NextFunction) => {
      try {
        response
          .status(200)
          .send(
            await new UserTypesController(request.user).deletePermission(
              parseInt(request.params.id),
              parseInt(request.params.resourceId)
            )
          );
      } catch (error: unknown) {
        next(error);
      }
    }
  );
}
