//Models
import Services from '../database/models/Services';
import Users from '../database/models/Users';
import Connection from '../database';
// Controllers
import Controller from './Controller';
// Libs
import moment from 'moment-timezone';
// Types
import MonthlyIncome from '../dashboard-types/MonthlyIncome';
import DailyRegistration from '../dashboard-types/DailyRegistration';
import ServicePerMonth from '../dashboard-types/ServicesPerMonth';
import YearlyIncome from '../dashboard-types/YearlyIncome';
import PaymentMethods from '../dashboard-types/PaymentMethods';

export default class DashboardsController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async monthlyIncome(): Promise<MonthlyIncome[]> {
    try {
      const connection = await Connection.getConnection();
      return connection
        .createQueryBuilder(Services, 'Services')
        .select(['MONTH(service_date) AS month', 'SUM(Services.value) AS income', 'ServiceTypes.name AS service_name'])
        .where(
          'Services.professional_email = :professional_email AND Services.service_date BETWEEN :date1 AND :date2 AND Services.status_id = :status_id',
          {
            professional_email: this.user.email,
            date1: moment().startOf('month').format('YYYY-MM-DD'),
            date2: moment().endOf('month').format('YYYY-MM-DD'),
            status_id: 3
          }
        )
        .innerJoin('Services.serviceType', 'ServiceTypes')
        .groupBy('MONTH(service_date)')
        .addGroupBy('ServiceTypes.id')
        .orderBy('Services.service_date')
        .getRawMany();
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async yearlyIncome(): Promise<YearlyIncome[]> {
    try {
      const connection = await Connection.getConnection();
      return connection
        .createQueryBuilder(Services, 'Services')
        .select(['SUM(Services.value) AS income', 'ServiceTypes.name AS service_name'])
        .where(
          'Services.professional_email = :professional_email AND Services.service_date BETWEEN :date1 AND :date2 AND Services.status_id = :status_id',
          {
            professional_email: this.user.email,
            date1: moment().startOf('year').format('YYYY-MM-DD'),
            date2: moment().endOf('year').format('YYYY-MM-DD'),
            status_id: 3
          }
        )
        .innerJoin('Services.serviceType', 'ServiceTypes')
        .groupBy('ServiceTypes.id')
        .getRawMany();
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async dailyRegistration(): Promise<DailyRegistration[]> {
    try {
      const connection = await Connection.getConnection();
      return connection
        .createQueryBuilder(Users, 'Users')
        .select(['COUNT(Users.email) AS quantity', 'UserTypes.name AS user_type', 'DAY(Users.created_at) AS day'])
        .innerJoin('Users.userType', 'UserTypes')
        .where('Users.created_at BETWEEN :date1 AND :date2', {
          date1: moment().startOf('month').format('YYYY-MM-DD'),
          date2: moment().endOf('month').format('YYYY-MM-DD')
        })
        .groupBy('DAY(Users.created_at)')
        .addGroupBy('UserTypes.id')
        .orderBy('Users.created_at')
        .getRawMany();
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async servicesPerMonth(): Promise<ServicePerMonth[]> {
    try {
      const connection = await Connection.getConnection();
      return connection
        .createQueryBuilder(Services, 'Services')
        .select(['MONTH(service_date) AS month', 'COUNT(Services.id) AS quantity', 'ServiceTypes.name AS service_name'])
        .where('Services.service_date BETWEEN :date1 AND :date2', {
          date1: moment().startOf('month').format('YYYY-MM-DD'),
          date2: moment().endOf('month').format('YYYY-MM-DD')
        })
        .innerJoin('Services.serviceType', 'ServiceTypes')
        .groupBy('MONTH(service_date)')
        .addGroupBy('ServiceTypes.id')
        .orderBy('Services.service_date')
        .getRawMany();
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async paymentMethods(): Promise<PaymentMethods[]> {
    try {
      const connection = await Connection.getConnection();
      return connection
        .createQueryBuilder(Services, 'Services')
        .select(['COUNT(Services.payment_method_id) AS quantity', 'PaymentMethods.name AS payment_name'])
        .where('Services.status_id = :status_id', { status_id: 3 })
        .innerJoin('Services.paymentMethod', 'PaymentMethods')
        .groupBy('Services.payment_method_id')
        .getRawMany();
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }
}
