import { Like } from 'typeorm';
// Controllers
import Controller from './Controller';
// Models
import Users from '../database/models/Users';
import OfferedServiceTypes from '../database/models/OfferedServiceTypes';
import Connection from '../database';
import ServiceTypes from '../database/models/ServiceTypes';
// Request types
import ServiceTypeRequest from '../request-types/ServiceTypeRequest';
// Exceptions
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
import InvalidParameterValueException from '../response-types/4XX/InvalidParameterValueException';

export default class ServiceTypesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(): Promise<ServiceTypes[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(ServiceTypes, { order: { name: 'ASC' } });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<ServiceTypes> {
    const connection = await Connection.getConnection();
    try {
      const serviceType = await connection.manager.findOne(ServiceTypes, { id });
      return serviceType
        ? serviceType
        : Promise.reject(new NotFoundException(`Tipo de serviço '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  async create(data: ServiceTypeRequest): Promise<ServiceTypes> {
    try {
      ServiceTypesController.validateFields(data);
      const serviceType = ServiceTypesController.mapFromRequest(data);
      serviceType.code = await ServiceTypesController.generateCode(data.name as string);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(ServiceTypes).insert(serviceType);
      return this.getOne(insertResult.identifiers[0].id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(id: number, data: ServiceTypeRequest): Promise<ServiceTypes> {
    try {
      await this.getOne(id);
      const serviceTypes = ServiceTypesController.mapFromRequest(data);
      const connection = await Connection.getConnection();
      await connection.getRepository(ServiceTypes).update({ id }, serviceTypes);
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(id: number): Promise<ServiceTypes> {
    try {
      const oldServiceType = await this.getOne(id);
      const connection = await Connection.getConnection();
      await connection.getRepository(ServiceTypes).delete({ id });
      return oldServiceType;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getProfessionals(searchTerm: string): Promise<Users[]> {
    const connection = await Connection.getConnection();
    try {
      const serviceTypes = await connection
        .createQueryBuilder(ServiceTypes, 'ServiceTypes')
        .select(['OfferedServiceTypes.professional_email'])
        .innerJoin(OfferedServiceTypes, 'OfferedServiceTypes', 'OfferedServiceTypes.service_type_id = ServiceTypes.id')
        .where(
          '(ServiceTypes.name LIKE :name OR ServiceTypes.code LIKE :name) AND OfferedServiceTypes.professional <> :email',
          {
            name: `${searchTerm}%`,
            email: this.user.email
          }
        )
        .getRawMany();

      if (serviceTypes.length === 0) return [];
      return connection
        .createQueryBuilder(Users, 'Users')
        .innerJoinAndSelect('Users.offeredServiceTypes', 'OfferedServiceTypes')
        .innerJoinAndSelect('OfferedServiceTypes.serviceType', 'ServiceTypes')
        .leftJoinAndSelect('Users.acceptedPaymentMethods', 'AcceptedPaymentMethods')
        .leftJoinAndSelect('AcceptedPaymentMethods.paymentMethod', 'PaymentMethods')
        .leftJoinAndSelect('Users.workedServices', 'Services')
        .leftJoinAndSelect('Services.review', 'Reviews')
        .where('Users.email IN (:...emails) AND Users.user_type_id = :type_id', {
          emails: serviceTypes.map((serviceType) => serviceType.professional_email),
          type_id: 2
        })
        .getMany();
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  static mapFromRequest(serviceRequest: ServiceTypeRequest): ServiceTypes {
    const serviceType = new ServiceTypes();
    if ('name' in serviceRequest && serviceRequest.name && ServiceTypesController.validateName(serviceRequest))
      serviceType.name = serviceRequest.name;

    return serviceType;
  }

  static validateFields(data: ServiceTypeRequest): void {
    if (!('name' in data)) throw new MissingParameterException(`Campo 'name' é obrigatório`);
  }

  static validateName({ name }: { name?: string }): boolean {
    ServiceTypesController.validateString(name as string, 'name');
    ServiceTypesController.validateEmptyString(name as string, 'name');
    return true;
  }

  static async generateCode(name: string): Promise<string> {
    try {
      if (name.length < 4) throw new InvalidParameterValueException('Name deve ter pelo menos 4 letras');

      const words = name.split(' ').filter((word: string) => word.length > 3);
      let prefix: string;
      if (words.length > 2) {
        words.splice(3);
        prefix = words
          .map((word: string) => word.charAt(0))
          .join('')
          .toUpperCase();
      } else prefix = words[0].substring(0, 3).toUpperCase();

      const suffix = await ServiceTypesController.getCodeSufix(prefix);

      return `${prefix}${suffix}`;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static async getCodeSufix(prefix: string): Promise<string> {
    try {
      const connection = await Connection.getConnection();
      const [, count] = await connection.manager.findAndCount(ServiceTypes, {
        where: {
          code: Like(`${prefix}%`)
        }
      });

      return `${count + 1}`.padStart(2, '0');
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }
}
