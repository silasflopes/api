import Connection from '../database';
import Users from '../database/models/Users';
import moment from 'moment-timezone';
import UnauthorizedException from '../response-types/4XX/UnauthorizedException';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import UsersController from './UsersController';

export default class SecurityController {
  async authenticate(credentials: string): Promise<[Users, string]> {
    if (typeof credentials !== 'string' || !credentials.startsWith('Basic '))
      throw new UnauthorizedException('Usuário não autenticado');

    credentials = credentials.replace('Basic ', '');

    const base64Decoded = Buffer.from(credentials, 'base64').toString('utf8');
    const email = base64Decoded.split(':')[0];
    const password = base64Decoded.split(':')[1];

    const connection = await Connection.getConnection();
    const user = await connection.manager.findOne(
      Users,
      {
        email,
        password: crypto
          .createHash('sha256')
          .update((process.env.SALT as string) + password)
          .digest('hex')
      },
      {
        relations: ['userType', 'userType.permissions', 'userType.permissions.resource']
      }
    );
    if (!user) throw new UnauthorizedException('Usuário e/ou senha incorretos');

    user.lastAccess = moment().tz('America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');
    const usersController = new UsersController(user);
    await usersController.update(email, UsersController.mapToRequest(user));
    const permissions = user.userType.permissions.map((permission) => permission.resource.name);
    user.userType.permissions = [];
    return [
      user,
      jwt.sign(
        {
          ...user,
          userType: { id: user.userType.id, name: user.userType.name },
          permissions
        },
        process.env.SECRET as string,
        { expiresIn: 24 * 60 * 60 }
      )
    ];
  }

  async facebookLogin({
    first_name,
    last_name,
    user_id
  }: {
    first_name: string;
    last_name: string;
    user_id: string;
  }): Promise<[Users, string]> {
    try {
      const connection = await Connection.getConnection();
      let user = await connection.manager.findOne(
        Users,
        {
          email: `${user_id}@hireme.com`
        },
        {
          relations: ['userType', 'userType.permissions', 'userType.permissions.resource']
        }
      );

      if (!user) {
        user = await new UsersController(new Users()).create({
          name: first_name,
          surname: last_name,
          email: `${user_id}@hireme.com`,
          birthdate: '2000-01-01',
          user_type_id: 3,
          password: UsersController.generatePassword()
        });
      }

      user.lastAccess = moment().tz('America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');
      const usersController = new UsersController(user);
      await usersController.update(user.email, UsersController.mapToRequest(user));
      const permissions = user.userType.permissions.map((permission) => permission.resource.name);
      return [
        user,
        jwt.sign(
          {
            ...user,
            userType: { id: user.userType.id, name: user.userType.name },
            permissions
          },
          process.env.SECRET as string,
          { expiresIn: 24 * 60 * 60 }
        )
      ];
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }
}
