// Controllers
import Controller from './Controller';
// Models
import Users from '../database/models/Users';
import Connection from '../database';
import Resources from '../database/models/Resources';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
// Request types
import ResourceRequest from '../request-types/ResourceRequest';

export default class ResourcesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(): Promise<Resources[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(Resources, { order: { name: 'ASC' } });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<Resources> {
    const connection = await Connection.getConnection();
    try {
      const resource = await connection.manager.findOne(Resources, { id });
      return resource ? resource : Promise.reject(new NotFoundException(`Recurso '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  async create(data: ResourceRequest): Promise<Resources> {
    try {
      ResourcesController.validateFields(data);
      const resource = ResourcesController.mapFromRequest(data);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(Resources).insert(resource);
      return this.getOne(insertResult.identifiers[0].id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(id: number, data: ResourceRequest): Promise<Resources> {
    try {
      await this.getOne(id);
      const resource = ResourcesController.mapFromRequest(data);
      const connection = await Connection.getConnection();
      await connection.getRepository(Resources).update({ id }, resource);
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(id: number): Promise<Resources> {
    try {
      const oldResource = await this.getOne(id);
      const connection = await Connection.getConnection();
      await connection.getRepository(Resources).delete({ id });
      return oldResource;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  static mapFromRequest(resourceRequest: ResourceRequest): Resources {
    const resource = new Resources();
    if ('name' in resourceRequest && resourceRequest.name && ResourcesController.validateName(resourceRequest))
      resource.name = resourceRequest.name;

    return resource;
  }

  static validateFields(data: ResourceRequest): void {
    if (!('name' in data)) throw new MissingParameterException(`Campo 'name' é obrigatório`);
  }

  static validateName({ name }: { name?: string }): boolean {
    ResourcesController.validateString(name as string, 'name');
    ResourcesController.validateEmptyString(name as string, 'name');
    return true;
  }
}
