// Request types
import ServiceRequest from '../request-types/ServiceRequest';
import AddressRequest from '../request-types/AddressRequest';
import ServiceAddressRequest from '../request-types/ServiceAddressRequest';
// Controllers
import Controller from './Controller';
import ServiceTypesController from './ServiceTypesController';
import PaymentMethodsController from './PaymentMethodsController';
import UsersController from './UsersController';
import StatusController from './StatusController';
import ServiceAddressesController from './ServiceAddressesController';
import AddressesController from './AddressesController';
// Models
import Services from '../database/models/Services';
import Connection from '../database';
import Users from '../database/models/Users';
// Libs
import moment from 'moment-timezone';
// Exceptions
import InvalidParameterValueException from '../response-types/4XX/InvalidParameterValueException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
import NotFoundException from '../response-types/4XX/NotFoundException';
import OperationNotPermittedException from '../response-types/4XX/OperationNotPermittedException';
export default class ServicesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(filters: ServiceRequest): Promise<Services[]> {
    try {
      const service = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(Services, {
        where: service,
        relations: [
          'serviceType',
          'paymentMethod',
          'user',
          'professional',
          'status',
          'review',
          'addresses',
          'review.user'
        ],
        order: {
          createdAt: 'DESC'
        }
      });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: string): Promise<Services> {
    const connection = await Connection.getConnection();
    try {
      const service = await connection.manager.findOne(Services, {
        where: { id },
        relations: [
          'serviceType',
          'paymentMethod',
          'user',
          'professional',
          'status',
          'review',
          'addresses',
          'review.user'
        ]
      });
      return service ? service : Promise.reject(new NotFoundException(`Serviço '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  public async create(data: ServiceRequest): Promise<Services> {
    try {
      ServicesController.validateFields(data);
      const service = await this.mapFromRequest(data);
      service.value = service.baseValue;
      service.status = await new StatusController(this.user).getOne(1);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(Services).insert(service);
      const id = insertResult.identifiers[0].id;
      if (data.addresses)
        await Promise.all(
          data.addresses.map((address: ServiceAddressRequest) => {
            const seviceAddressRequest = {
              ...address,
              address_id: address.id,
              service_id: id
            };
            delete seviceAddressRequest.id;
            return new ServiceAddressesController(this.user).create(seviceAddressRequest);
          })
        );
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(id: string, data: ServiceRequest, editByPartner: boolean): Promise<Services> {
    try {
      const oldService = await this.getOne(id);
      const service = await this.mapFromRequest(data);

      if (service.status && service.status.id !== 2)
        if (this.user.email === oldService.user.email) {
          if (
            typeof oldService.userValueAcceptance === 'number' &&
            typeof oldService.professionalValueAcceptance !== 'number' &&
            !('professional_acceptance' in data)
          )
            throw new OperationNotPermittedException('Operação não permitida. Aguarde a aceitação da proposta');
          if (
            typeof oldService.professionalValueAcceptance === 'number' &&
            typeof oldService.userValueAcceptance !== 'number' &&
            !('user_acceptance' in data)
          )
            throw new OperationNotPermittedException('Operação não permitida. Você deve aceitar ou recusar a proposta');
        } else {
          if (
            typeof oldService.professionalValueAcceptance === 'number' &&
            typeof oldService.userValueAcceptance !== 'number' &&
            !('user_acceptance' in data)
          )
            throw new OperationNotPermittedException('Operação não permitida. Aguarde a aceitação da proposta');
          if (
            typeof oldService.userValueAcceptance === 'number' &&
            typeof oldService.professionalValueAcceptance !== 'number' &&
            !('professional_acceptance' in data)
          )
            throw new OperationNotPermittedException('Operação não permitida. Você deve aceitar ou recusar a proposta');
        }

      if ([2, 3].includes(oldService.status.id))
        throw new OperationNotPermittedException('Operação não permitida. O serviço já foi encerrado');

      if (editByPartner) {
        if (oldService.professional.email !== this.user.email)
          throw new OperationNotPermittedException('Operação não permitida');
        if (service.description) throw new OperationNotPermittedException('Proibido editar a descrição');
      } else if (oldService.user.email !== this.user.email)
        throw new OperationNotPermittedException('Operação não permitida');

      if ('user_acceptance' in data || 'professional_acceptance' in data) {
        if (
          typeof oldService.userValueAcceptance === 'number' &&
          'professional_acceptance' in data &&
          !data.professional_acceptance
        ) {
          service.professionalValueAcceptance = null;
          service.userValueAcceptance = null;
        }
        if (
          typeof oldService.professionalValueAcceptance === 'number' &&
          'user_acceptance' in data &&
          !data.user_acceptance
        ) {
          service.professionalValueAcceptance = null;
          service.userValueAcceptance = null;
        }
      }

      if (service.status && [2, 3].includes(service.status.id))
        service.finishedAt = moment().tz('America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');

      const connection = await Connection.getConnection();
      await connection.getRepository(Services).update({ id }, service);
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(id: string): Promise<Services> {
    try {
      const oldService = await this.getOne(id);
      const connection = await Connection.getConnection();
      await connection.getRepository(Services).delete({ id });
      return oldService;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  static validateFields(data: ServiceRequest): void {
    if (!('service_type_id' in data)) throw new MissingParameterException(`Campo 'service_type_id' é obrigatório`);
    if (!('payment_method_id' in data)) throw new MissingParameterException(`Campo 'payment_method_id' é obrigatório`);
    if (!('user_email' in data)) throw new MissingParameterException(`Campo 'user_email' é obrigatório`);
    if (!('professional_email' in data))
      throw new MissingParameterException(`Campo 'professional_email' é obrigatório`);
    if (!('base_value' in data)) throw new MissingParameterException(`Campo 'base_value' é obrigatório`);
    if (!('service_date' in data)) throw new MissingParameterException(`Campo 'service_date' é obrigatório`);
    if (!('description' in data)) throw new MissingParameterException(`Campo 'description' é obrigatório`);
  }

  async mapFromRequest(serviceRequest: ServiceRequest): Promise<Services> {
    try {
      const service = new Services();
      if (
        'service_type_id' in serviceRequest &&
        serviceRequest.service_type_id &&
        ServicesController.validateServiceTypeId(serviceRequest)
      )
        service.serviceType = await new ServiceTypesController(this.user).getOne(serviceRequest.service_type_id);
      if (
        'payment_method_id' in serviceRequest &&
        serviceRequest.payment_method_id &&
        ServicesController.validatePaymentMethodId(serviceRequest)
      )
        service.paymentMethod = await new PaymentMethodsController(this.user).getOne(serviceRequest.payment_method_id);
      if (
        'user_email' in serviceRequest &&
        serviceRequest.user_email &&
        UsersController.validateUserEmail({ email: serviceRequest.user_email })
      )
        service.user = await new UsersController(this.user).getOne(serviceRequest.user_email);
      if (
        'professional_email' in serviceRequest &&
        serviceRequest.professional_email &&
        UsersController.validateUserEmail({ email: serviceRequest.professional_email })
      )
        service.professional = await new UsersController(this.user).getOne(serviceRequest.professional_email);
      if (
        'base_value' in serviceRequest &&
        serviceRequest.base_value &&
        ServicesController.validateBaseValue(serviceRequest)
      )
        service.baseValue = serviceRequest.base_value;
      if (
        'service_date' in serviceRequest &&
        serviceRequest.service_date &&
        ServicesController.validateServiceDate(serviceRequest)
      )
        service.serviceDate = serviceRequest.service_date;
      if (
        'description' in serviceRequest &&
        serviceRequest.description &&
        ServicesController.validateDescription(serviceRequest)
      )
        service.description = serviceRequest.description;
      if ('value' in serviceRequest && serviceRequest.value && ServicesController.validateValue(serviceRequest))
        service.value = serviceRequest.value;
      if (
        'status_id' in serviceRequest &&
        serviceRequest.status_id &&
        ServicesController.validateStatusId(serviceRequest)
      )
        service.status = await new StatusController(this.user).getOne(serviceRequest.status_id);
      if ('id' in serviceRequest && serviceRequest.id && ServicesController.validateId(serviceRequest))
        service.id = serviceRequest.id;
      if (
        'professional_acceptance' in serviceRequest &&
        ServicesController.validateProfessionalAcceptance(serviceRequest)
      )
        service.professionalValueAcceptance = serviceRequest.professional_acceptance;
      if ('user_acceptance' in serviceRequest && ServicesController.validateUserAcceptance(serviceRequest))
        service.userValueAcceptance = serviceRequest.user_acceptance;

      return service;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static validateId({ id }: { id?: string }): boolean {
    ServicesController.validateString(id as string, 'id');
    ServicesController.validateEmptyString(id as string, 'id');
    return true;
  }

  public static validateServiceTypeId({ service_type_id }: { service_type_id?: number }): boolean {
    ServicesController.validateNumber(service_type_id as number, 'service_type_id');
    return true;
  }
  public static validatePaymentMethodId({ payment_method_id }: { payment_method_id?: number }): boolean {
    ServicesController.validateNumber(payment_method_id as number, 'payment_method_id');
    return true;
  }
  public static validateBaseValue({ base_value }: { base_value?: number }): boolean {
    ServicesController.validateNumber(base_value as number, 'base_value');
    return true;
  }
  public static validateServiceDate({ service_date }: { service_date?: string }): boolean {
    ServicesController.validateString(service_date as string, 'service_date');
    ServicesController.validateEmptyString(service_date as string, 'service_date');
    ServicesController.validateDate(service_date as string);
    const today = moment(moment().format('YYYY-MM-DD')).tz('America/Sao_Paulo');
    const inputDate = moment(service_date).tz('America/Sao_Paulo');
    if (today.diff(inputDate, 'day') > 0)
      throw new InvalidParameterValueException('Data do serviço não pode ser no passado');

    return true;
  }
  public static validateDescription({ description }: { description?: string }): boolean {
    ServicesController.validateString(description as string, 'description');
    ServicesController.validateEmptyString(description as string, 'description');
    return true;
  }
  public static validateValue({ value }: { value?: number }): boolean {
    ServicesController.validateNumber(value as number, 'value');
    return true;
  }
  public static validateProfessionalAcceptance({
    professional_acceptance
  }: {
    professional_acceptance?: boolean;
  }): boolean {
    ServicesController.validateBoolean(professional_acceptance as boolean, 'value');
    return true;
  }
  public static validateUserAcceptance({ user_acceptance }: { user_acceptance?: boolean }): boolean {
    ServicesController.validateBoolean(user_acceptance as boolean, 'value');
    return true;
  }
  public static validateStatusId({ status_id }: { status_id?: number }): boolean {
    ServicesController.validateNumber(status_id as number, 'status_id');
    return true;
  }
  public validateAddresses({ addresses }: { addresses: AddressRequest[] }): boolean {
    ServicesController.validateArray(addresses, 'addresses');
    addresses.forEach(
      async (address: AddressRequest) => await new AddressesController(this.user).mapFromRequest(address)
    );
    return true;
  }
}
