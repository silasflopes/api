// Models
import Connection from '../database';
import Users from '../database/models/Users';
import AccessLevel from '../database/models/AccessLevel';
// Controllers
import Controller from './Controller';
import ResourcesController from './ResourcesController';
import UserTypesController from './UserTypesController';
// Request Types
import AccessLevelRequest from 'src/request-types/AccessLevelRequest';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';

export default class AccessLevelController extends Controller {
  constructor(user: Users) {
    super(user);
  }
  async get(filters: AccessLevelRequest): Promise<AccessLevel[]> {
    try {
      const accessLevel = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(AccessLevel, { where: accessLevel, relations: ['resource'] });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<AccessLevel> {
    try {
      const connection = await Connection.getConnection();
      const accessLevel = await connection.manager.findOne(AccessLevel, { id });
      return accessLevel
        ? accessLevel
        : Promise.reject(new NotFoundException(`Nível de acesso '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async create(data: AccessLevelRequest): Promise<AccessLevel> {
    try {
      const accessLevel = await this.mapFromRequest(data);
      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(AccessLevel).insert(accessLevel);
      const id = insertResult.identifiers[0].id;
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(id: number): Promise<AccessLevel> {
    try {
      const oldAccessLevel = await new AccessLevelController(this.user).getOne(id);
      const connection = await Connection.getConnection();
      await connection.getRepository(AccessLevel).delete({ id });
      return oldAccessLevel;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(accessLevelRequest: AccessLevelRequest): Promise<AccessLevel> {
    try {
      const accessLevel = new AccessLevel();
      if (
        'resource_id' in accessLevelRequest &&
        accessLevelRequest.resource_id &&
        AccessLevelController.validateResourceId(accessLevelRequest)
      )
        accessLevel.resource = await new ResourcesController(this.user).getOne(accessLevelRequest.resource_id);
      if (
        'user_type_id' in accessLevelRequest &&
        accessLevelRequest.user_type_id &&
        AccessLevelController.validateUserTypeId(accessLevelRequest)
      )
        accessLevel.userType = await new UserTypesController(this.user).getOne(accessLevelRequest.user_type_id);

      return accessLevel;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static validateResourceId({ resource_id }: { resource_id?: number }): boolean {
    AccessLevelController.validateNumber(resource_id as number, 'resource_id');
    return true;
  }
  static validateUserTypeId({ user_type_id }: { user_type_id: number }): boolean {
    AccessLevelController.validateNumber(user_type_id, 'user_type_id');
    return true;
  }
}
