// Controllers
import Controller from './Controller';
// Models
import Connection from '../database';
import Users from '../database/models/Users';
import Status from '../database/models/Status';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';

export default class StatusController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(): Promise<Status[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(Status);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<Status> {
    const connection = await Connection.getConnection();
    try {
      const status = await connection.manager.findOne(Status, { id });
      return status ? status : Promise.reject(new NotFoundException(`Status '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }
}
