// Controllers
import Controller from './Controller';
// Models
import Users from '../database/models/Users';
import Connection from '../database';
import Messages from '../database/models/Messages';
import ServicesController from './ServicesController';
import UsersController from './UsersController';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
// Request types
import MessageRequest from '../request-types/MessageRequest';

export default class MessagesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async getOne(id: number): Promise<Messages> {
    const connection = await Connection.getConnection();
    try {
      const message = await connection.manager.findOne(Messages, { id });
      return message ? message : Promise.reject(new NotFoundException(`Mensagem '${id}' não encontrada`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async get(filters: MessageRequest): Promise<Messages[]> {
    try {
      const message = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(Messages, { where: message, relations: ['from'] });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async create(data: MessageRequest): Promise<Messages> {
    try {
      MessagesController.validateFields(data);
      const message = await this.mapFromRequest(data);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(Messages).insert(message);
      return this.getOne(insertResult.identifiers[0].id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(messageRequest: MessageRequest): Promise<Messages> {
    try {
      const message = new Messages();
      if ('message' in messageRequest && messageRequest.message && MessagesController.validateMessage(messageRequest))
        message.message = messageRequest.message;
      if (
        'service_id' in messageRequest &&
        messageRequest.service_id &&
        MessagesController.validateServiceId(messageRequest)
      )
        message.service = await new ServicesController(this.user).getOne(messageRequest.service_id);
      if ('from' in messageRequest && messageRequest.from && MessagesController.validateFrom(messageRequest))
        message.from = await new UsersController(this.user).getOne(messageRequest.from);
      if ('id' in messageRequest && messageRequest.id && MessagesController.validateId(messageRequest))
        message.id = messageRequest.id;

      return message;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static validateId({ id }: { id?: number }): boolean {
    MessagesController.validateNumber(id as number, 'id');
    return true;
  }

  static validateFields(data: MessageRequest): void {
    if (!('message' in data)) throw new MissingParameterException(`Campo 'message' é obrigatório`);
  }

  static validateMessage({ message }: { message?: string }): boolean {
    MessagesController.validateString(message as string, 'message');
    MessagesController.validateEmptyString(message as string, 'message');
    return true;
  }
  static validateServiceId({ service_id }: { service_id?: string }): boolean {
    MessagesController.validateString(service_id as string, 'service_id');
    MessagesController.validateEmptyString(service_id as string, 'service_id');
    return true;
  }
  static validateFrom({ from }: { from?: string }): boolean {
    MessagesController.validateString(from as string, 'from');
    MessagesController.validateEmptyString(from as string, 'from');
    return true;
  }
}
