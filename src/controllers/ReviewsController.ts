// Controllers
import Controller from './Controller';
import UsersController from './UsersController';
// Models
import Users from '../database/models/Users';
import Connection from '../database';
import Reviews from '../database/models/Reviews';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';
import InvalidParameterValueException from '../response-types/4XX/InvalidParameterValueException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
import OperationNotPermittedException from '../response-types/4XX/OperationNotPermittedException';
// Request Types
import ReviewRequest from '../request-types/ReviewRequest';
import ServicesController from './ServicesController';

export default class ReviewsController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async getOne(id: string): Promise<Reviews> {
    const connection = await Connection.getConnection();
    try {
      const review = await connection.manager.findOne(Reviews, { where: { serviceId: id }, relations: ['user'] });
      return review ? review : Promise.reject(new NotFoundException(`Avaliação '${id}' não encontrada`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async create(data: ReviewRequest): Promise<Reviews> {
    try {
      ReviewsController.validateFields(data);
      const review = await this.mapFromRequest(data);
      const service = await new ServicesController(this.user).getOne(data.service_id as string);

      if (data.user_email !== service.user.email) throw new OperationNotPermittedException('Operação não permitida');
      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(Reviews).insert(review);
      return this.getOne(insertResult.identifiers[0].serviceId);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(reviewRequest: ReviewRequest): Promise<Reviews> {
    const review = new Reviews();
    if ('comment' in reviewRequest && reviewRequest.comment && ReviewsController.validateComment(reviewRequest))
      review.comment = reviewRequest.comment;
    if (
      'service_id' in reviewRequest &&
      reviewRequest.service_id &&
      ReviewsController.validateServiceId(reviewRequest)
    ) {
      review.service = await new ServicesController(this.user).getOne(reviewRequest.service_id);
      review.serviceId = reviewRequest.service_id;
    }
    if ('user_email' in reviewRequest && reviewRequest.user_email && ReviewsController.validateUserEmail(reviewRequest))
      review.user = await new UsersController(this.user).getOne(reviewRequest.user_email);
    if ('rate' in reviewRequest && reviewRequest.rate && ReviewsController.validateRate(reviewRequest))
      review.rate = reviewRequest.rate;

    return review;
  }

  static validateFields(data: ReviewRequest): void {
    if (!('rate' in data)) throw new MissingParameterException(`Campo 'rate' é obrigatório`);
    if (!('service_id' in data)) throw new MissingParameterException(`Campo 'service_id' é obrigatório`);
    if (!('user_email' in data)) throw new MissingParameterException(`Campo 'user_email' é obrigatório`);
  }

  static validateComment({ comment }: { comment?: string }): boolean {
    ReviewsController.validateString(comment as string, 'comment');
    return true;
  }
  static validateServiceId({ service_id }: { service_id?: string }): boolean {
    ReviewsController.validateString(service_id as string, 'service_id');
    ReviewsController.validateEmptyString(service_id as string, 'service_id');
    return true;
  }
  static validateUserEmail({ user_email }: { user_email?: string }): boolean {
    ReviewsController.validateString(user_email as string, 'user_email');
    ReviewsController.validateEmptyString(user_email as string, 'user_email');
    return true;
  }
  static validateRate({ rate }: { rate?: number }): boolean {
    ReviewsController.validateNumber(rate as number, 'rate');
    if (typeof rate === 'number' && (rate < 1 || rate > 5))
      throw new InvalidParameterValueException('A avaliação do serviço deve ter entre 1 e 5 estrelas');
    return true;
  }
}
