// Request types
import ServiceAddressRequest from '../request-types/ServiceAddressRequest';
// Controllers
import Controller from './Controller';
import AddressesController from './AddressesController';
import ServicesController from './ServicesController';
// Models
import Connection from '../database';
import ServiceAddresses from '../database/models/ServiceAddresses';
import Users from '../database/models/Users';
// Libs
// Exceptions
import MissingParameterException from '../response-types/4XX/MissingParameterException';
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';

export default class ServiceAddressesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(filters: ServiceAddressRequest): Promise<ServiceAddresses[]> {
    try {
      const address = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(ServiceAddresses, address);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<ServiceAddresses> {
    try {
      const connection = await Connection.getConnection();
      const address = await connection.manager.findOne(ServiceAddresses, { id });
      return address ? address : Promise.reject(new NotFoundException(`Endereço '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  async create(data: ServiceAddressRequest): Promise<ServiceAddresses> {
    try {
      ServiceAddressesController.validateFields(data);
      const service = await this.mapFromRequest(data);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(ServiceAddresses).insert(service);
      return this.getOne(insertResult.identifiers[0].id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteMany(filters: ServiceAddressRequest): Promise<ServiceAddresses[]> {
    try {
      const serviceAddress = await this.mapFromRequest(filters);
      const oldAddresses = await this.get(filters);
      const connection = await Connection.getConnection();
      await connection.getRepository(ServiceAddresses).delete(serviceAddress);
      return oldAddresses;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(serviceAddressRequest: ServiceAddressRequest): Promise<ServiceAddresses> {
    try {
      const serviceAddress = new ServiceAddresses();
      if (
        'name' in serviceAddressRequest &&
        serviceAddressRequest.name &&
        AddressesController.validateName(serviceAddressRequest)
      )
        serviceAddress.name = serviceAddressRequest.name;
      if (
        'address' in serviceAddressRequest &&
        serviceAddressRequest.address &&
        AddressesController.validateAddress(serviceAddressRequest)
      )
        serviceAddress.addressName = serviceAddressRequest.address;
      if (
        'address_id' in serviceAddressRequest &&
        serviceAddressRequest.address_id &&
        ServiceAddressesController.validateAddressId(serviceAddressRequest)
      )
        serviceAddress.address = await new AddressesController(this.user).getOne(serviceAddressRequest.address_id);
      if (
        'service_id' in serviceAddressRequest &&
        serviceAddressRequest.service_id &&
        ServiceAddressesController.validateServiceId(serviceAddressRequest)
      )
        serviceAddress.service = await new ServicesController(this.user).getOne(serviceAddressRequest.service_id);
      if (
        'number' in serviceAddressRequest &&
        serviceAddressRequest.number &&
        AddressesController.validateHouseNumber(serviceAddressRequest)
      )
        serviceAddress.number = serviceAddressRequest.number;
      if (
        'complement' in serviceAddressRequest &&
        serviceAddressRequest.complement &&
        AddressesController.validateComplement(serviceAddressRequest)
      )
        serviceAddress.complement = serviceAddressRequest.complement;
      if (
        'district' in serviceAddressRequest &&
        serviceAddressRequest.district &&
        AddressesController.validateDistrict(serviceAddressRequest)
      )
        serviceAddress.district = serviceAddressRequest.district;
      if (
        'city' in serviceAddressRequest &&
        serviceAddressRequest.city &&
        AddressesController.validateCity(serviceAddressRequest)
      )
        serviceAddress.city = serviceAddressRequest.city;
      if (
        'state' in serviceAddressRequest &&
        serviceAddressRequest.state &&
        AddressesController.validateState(serviceAddressRequest)
      )
        serviceAddress.state = serviceAddressRequest.state;
      if (
        'country' in serviceAddressRequest &&
        serviceAddressRequest.country &&
        AddressesController.validateCountry(serviceAddressRequest)
      )
        serviceAddress.country = serviceAddressRequest.country;
      if (
        'zipcode' in serviceAddressRequest &&
        serviceAddressRequest.zipcode &&
        AddressesController.validateZipcode(serviceAddressRequest)
      )
        serviceAddress.zipcode = serviceAddressRequest.zipcode;
      if (
        'id' in serviceAddressRequest &&
        serviceAddressRequest.id &&
        AddressesController.validateId(serviceAddressRequest)
      )
        serviceAddress.id = serviceAddressRequest.id;

      return serviceAddress;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static validateFields(data: ServiceAddressRequest): void {
    if (!('address_id' in data)) throw new MissingParameterException(`Campo 'address_id' é obrigatório`);
    if (!('service_id' in data)) throw new MissingParameterException(`Campo 'service_id' é obrigatório`);
    if (!('name' in data)) throw new MissingParameterException(`Campo 'name' é obrigatório`);
    if (!('address' in data)) throw new MissingParameterException(`Campo 'address' é obrigatório`);
    if (!('number' in data)) throw new MissingParameterException(`Campo 'number' é obrigatório`);
    if (!('district' in data)) throw new MissingParameterException(`Campo 'district' é obrigatório`);
    if (!('city' in data)) throw new MissingParameterException(`Campo 'city' é obrigatório`);
    if (!('state' in data)) throw new MissingParameterException(`Campo 'state' é obrigatório`);
    if (!('country' in data)) throw new MissingParameterException(`Campo 'country' é obrigatório`);
    if (!('zipcode' in data)) throw new MissingParameterException(`Campo 'zipcode' é obrigatório`);
  }

  public static validateServiceId({ service_id }: { service_id?: string }): boolean {
    ServiceAddressesController.validateString(service_id as string, 'service_id');
    ServiceAddressesController.validateEmptyString(service_id as string, 'service_id');
    return true;
  }
  public static validateAddressId({ address_id }: { address_id?: number }): boolean {
    ServiceAddressesController.validateNumber(address_id as number, 'address_id');
    return true;
  }
}
