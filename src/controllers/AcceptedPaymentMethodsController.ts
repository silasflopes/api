// Controllers
import Controller from './Controller';
import UsersController from './UsersController';
import PaymentMethodsController from './PaymentMethodsController';
// Models
import Users from 'src/database/models/Users';
import Connection from '../database';
import AcceptedPaymentMethods from '../database/models/AcceptedPaymentMethods';
// Response types
import NotFoundException from '../response-types/4XX/NotFoundException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
// Request types
import AcceptedPaymentMethodRequest from '../request-types/AcceptedPaymentMethodRequest';

export default class AcceptedPaymentMethodsController extends Controller {
  constructor(user: Users) {
    super(user);
  }
  async get(filters: AcceptedPaymentMethodRequest): Promise<AcceptedPaymentMethods[]> {
    try {
      const acceptedPaymentMethod = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(AcceptedPaymentMethods, {
        where: acceptedPaymentMethod,
        relations: ['paymentMethod']
      });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<AcceptedPaymentMethods> {
    try {
      const connection = await Connection.getConnection();
      const acceptedPaymentMethods = await connection.manager.findOne(AcceptedPaymentMethods, { id });
      return acceptedPaymentMethods
        ? acceptedPaymentMethods
        : Promise.reject(new NotFoundException(`Forma de pagamento '${id}' não encontrada`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async createBatch(data: AcceptedPaymentMethodRequest[]): Promise<AcceptedPaymentMethods[]> {
    try {
      return Promise.all(data.map((data: AcceptedPaymentMethodRequest) => this.create(data)));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async create(data: AcceptedPaymentMethodRequest): Promise<AcceptedPaymentMethods> {
    try {
      AcceptedPaymentMethodsController.validateFields(data);
      const connection = await Connection.getConnection();
      const acceptedPaymentMethod = await this.mapFromRequest(data);

      await connection.getRepository(AcceptedPaymentMethods).insert(acceptedPaymentMethod);
      return this.getOne(acceptedPaymentMethod.id);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async deleteAll(professional_email: string): Promise<void> {
    try {
      const connection = await Connection.getConnection();
      await connection
        .getRepository(AcceptedPaymentMethods)
        .delete({ professional: await new UsersController(this.user).getOne(professional_email) });
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(acceptedPaymentMethodRequest: AcceptedPaymentMethodRequest): Promise<AcceptedPaymentMethods> {
    const acceptedPaymentMethod = new AcceptedPaymentMethods();
    try {
      if (
        'payment_method_id' in acceptedPaymentMethodRequest &&
        acceptedPaymentMethodRequest.payment_method_id &&
        AcceptedPaymentMethodsController.validatePaymentMethodId(acceptedPaymentMethodRequest)
      )
        acceptedPaymentMethod.paymentMethod = await new PaymentMethodsController(this.user).getOne(
          acceptedPaymentMethodRequest.payment_method_id
        );
      if (
        'professional_email' in acceptedPaymentMethodRequest &&
        acceptedPaymentMethodRequest.professional_email &&
        UsersController.validateUserEmail({ email: acceptedPaymentMethodRequest.professional_email })
      )
        acceptedPaymentMethod.professional = await new UsersController(this.user).getOne(
          acceptedPaymentMethodRequest.professional_email
        );
    } catch (error: unknown) {
      return Promise.reject(error);
    }
    return acceptedPaymentMethod;
  }

  static validateFields(data: AcceptedPaymentMethodRequest): void {
    if (!('payment_method_id' in data)) throw new MissingParameterException(`Campo 'payment_method_id' é obrigatório`);
    if (!('professional_email' in data))
      throw new MissingParameterException(`Campo 'professional_email' é obrigatório`);
  }

  static validatePaymentMethodId({ payment_method_id }: { payment_method_id?: number }): boolean {
    AcceptedPaymentMethodsController.validateNumber(payment_method_id as number, 'payment_method_id');
    return true;
  }
}
