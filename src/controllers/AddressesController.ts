// Models
import Connection from '../database';
import Addresses from '../database/models/Addresses';
import Users from '../database/models/Users';
// Controllers
import Controller from './Controller';
// Libs
// Request types
import AddressRequest from '../request-types/AddressRequest';
// Exceptions
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
import UsersController from './UsersController';

export default class AddressesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(filters: AddressRequest): Promise<Addresses[]> {
    try {
      const address = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(Addresses, address);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<Addresses> {
    const connection = await Connection.getConnection();
    try {
      const address = await connection.manager.findOne(Addresses, { id });
      return address ? address : Promise.reject(new NotFoundException(`Endereço '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  async create(data: AddressRequest): Promise<Addresses> {
    try {
      AddressesController.validateFields(data);
      const address = await this.mapFromRequest(data);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(Addresses).insert(address);
      return this.getOne(insertResult.identifiers[0].id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(id: number, data: AddressRequest): Promise<Addresses> {
    try {
      await this.getOne(id);
      const address = await this.mapFromRequest(data);
      const connection = await Connection.getConnection();
      await connection.getRepository(Addresses).update({ id }, address);
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(id: number): Promise<Addresses> {
    try {
      const oldAddress = await this.getOne(id);
      const connection = await Connection.getConnection();
      await connection.getRepository(Addresses).delete({ id });
      return oldAddress;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(addressRequest: AddressRequest): Promise<Addresses> {
    try {
      const address = new Addresses();
      if ('name' in addressRequest && addressRequest.name && AddressesController.validateName(addressRequest))
        address.name = addressRequest.name;
      if ('address' in addressRequest && addressRequest.address && AddressesController.validateAddress(addressRequest))
        address.address = addressRequest.address;
      if (
        'number' in addressRequest &&
        addressRequest.number &&
        AddressesController.validateHouseNumber(addressRequest)
      )
        address.number = addressRequest.number;
      if (
        'complement' in addressRequest &&
        addressRequest.complement &&
        AddressesController.validateComplement(addressRequest)
      )
        address.complement = addressRequest.complement;
      if (
        'district' in addressRequest &&
        addressRequest.district &&
        AddressesController.validateDistrict(addressRequest)
      )
        address.district = addressRequest.district;
      if ('city' in addressRequest && addressRequest.city && AddressesController.validateCity(addressRequest))
        address.city = addressRequest.city;
      if ('state' in addressRequest && addressRequest.state && AddressesController.validateState(addressRequest))
        address.state = addressRequest.state;
      if ('country' in addressRequest && addressRequest.country && AddressesController.validateCountry(addressRequest))
        address.country = addressRequest.country;
      if ('zipcode' in addressRequest && addressRequest.zipcode && AddressesController.validateZipcode(addressRequest))
        address.zipcode = addressRequest.zipcode;
      if (
        'user_email' in addressRequest &&
        addressRequest.user_email &&
        AddressesController.validateUserEmail(addressRequest)
      )
        address.user = await new UsersController(this.user).getOne(addressRequest.user_email);
      if ('id' in addressRequest && addressRequest.id && AddressesController.validateId(addressRequest))
        address.id = addressRequest.id;

      return address;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static validateFields(data: AddressRequest): void {
    if (!('name' in data)) throw new MissingParameterException(`Campo 'name' é obrigatório`);
    if (!('address' in data)) throw new MissingParameterException(`Campo 'address' é obrigatório`);
    if (!('number' in data)) throw new MissingParameterException(`Campo 'number' é obrigatório`);
    if (!('district' in data)) throw new MissingParameterException(`Campo 'district' é obrigatório`);
    if (!('city' in data)) throw new MissingParameterException(`Campo 'city' é obrigatório`);
    if (!('state' in data)) throw new MissingParameterException(`Campo 'state' é obrigatório`);
    if (!('country' in data)) throw new MissingParameterException(`Campo 'country' é obrigatório`);
    if (!('zipcode' in data)) throw new MissingParameterException(`Campo 'zipcode' é obrigatório`);
  }

  static validateName({ name }: { name?: string }): boolean {
    AddressesController.validateString(name as string, 'name');
    AddressesController.validateEmptyString(name as string, 'name');
    return true;
  }
  static validateAddress({ address }: { address?: string }): boolean {
    AddressesController.validateString(address as string, 'address');
    AddressesController.validateEmptyString(address as string, 'address');
    return true;
  }
  static validateHouseNumber({ number }: { number?: number }): boolean {
    AddressesController.validateNumber(number as number, 'number');
    return true;
  }
  static validateComplement({ complement }: { complement?: string }): boolean {
    AddressesController.validateString(complement as string, 'complement');
    AddressesController.validateEmptyString(complement as string, 'complement');
    return true;
  }
  static validateDistrict({ district }: { district?: string }): boolean {
    AddressesController.validateString(district as string, 'district');
    AddressesController.validateEmptyString(district as string, 'district');
    return true;
  }
  static validateCity({ city }: { city?: string }): boolean {
    AddressesController.validateString(city as string, 'city');
    AddressesController.validateEmptyString(city as string, 'city');
    return true;
  }
  static validateState({ state }: { state?: string }): boolean {
    AddressesController.validateString(state as string, 'state');
    AddressesController.validateEmptyString(state as string, 'state');
    return true;
  }
  static validateCountry({ country }: { country?: string }): boolean {
    AddressesController.validateString(country as string, 'country');
    AddressesController.validateEmptyString(country as string, 'country');
    return true;
  }
  static validateZipcode({ zipcode }: { zipcode?: string }): boolean {
    AddressesController.validateString(zipcode as string, 'zipcode');
    AddressesController.validateEmptyString(zipcode as string, 'zipcode');
    return true;
  }
  static validateUserEmail({ user_email }: { user_email?: string }): boolean {
    AddressesController.validateString(user_email as string, 'user_email');
    AddressesController.validateEmail(user_email as string);
    return true;
  }
  static validateId({ id }: { id?: number }): boolean {
    AddressesController.validateNumber(id as number, 'id');
    return true;
  }
}
