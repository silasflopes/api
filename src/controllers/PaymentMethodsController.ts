// Controller
import Controller from './Controller';
// Models
import Connection from '../database';
import PaymentMethods from '../database/models/PaymentMethods';
import Users from '../database/models/Users';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
// Request Types
import PaymentMethodRequest from '../request-types/PaymentMethodRequest';

export default class PaymentMethodsController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(): Promise<PaymentMethods[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(PaymentMethods, { order: { name: 'ASC' } });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<PaymentMethods> {
    const connection = await Connection.getConnection();
    try {
      const paymentMethod = await connection.manager.findOne(PaymentMethods, { id });
      return paymentMethod
        ? paymentMethod
        : Promise.reject(new NotFoundException(`Tipo de pagamento '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  async create(data: PaymentMethodRequest): Promise<PaymentMethods> {
    try {
      PaymentMethodsController.validateFields(data);
      const resource = PaymentMethodsController.mapFromRequest(data);

      const connection = await Connection.getConnection();
      const insertResult = await connection.getRepository(PaymentMethods).insert(resource);
      return this.getOne(insertResult.identifiers[0].id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(id: number, data: PaymentMethodRequest): Promise<PaymentMethods> {
    try {
      await this.getOne(id);
      const resource = PaymentMethodsController.mapFromRequest(data);
      const connection = await Connection.getConnection();
      await connection.getRepository(PaymentMethods).update({ id }, resource);
      return this.getOne(id);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(id: number): Promise<PaymentMethods> {
    try {
      const oldResource = await this.getOne(id);
      const connection = await Connection.getConnection();
      await connection.getRepository(PaymentMethods).delete({ id });
      return oldResource;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  static mapFromRequest(resourceRequest: PaymentMethodRequest): PaymentMethods {
    const serviceType = new PaymentMethods();
    if ('name' in resourceRequest && resourceRequest.name && PaymentMethodsController.validateName(resourceRequest))
      serviceType.name = resourceRequest.name;

    return serviceType;
  }

  static validateFields(data: PaymentMethodRequest): void {
    if (!('name' in data)) throw new MissingParameterException(`Campo 'name' é obrigatório`);
  }

  static validateName({ name }: { name?: string }): boolean {
    PaymentMethodsController.validateString(name as string, 'name');
    PaymentMethodsController.validateEmptyString(name as string, 'name');
    return true;
  }
}
