// Controllers
import Controller from './Controller';
import UsersController from './UsersController';
import ServiceTypesController from './ServiceTypesController';
// Models
import Users from '../database/models/Users';
import Connection from '../database';
import OfferedServiceTypes from '../database/models/OfferedServiceTypes';
// Response Types
import NotFoundException from '../response-types/4XX/NotFoundException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
// Request Types
import OfferedServiceTypesRequest from '../request-types/OfferedServiceTypeRequest';

export default class OfferedServiceTypesController extends Controller {
  constructor(user: Users) {
    super(user);
  }
  async get(filters: OfferedServiceTypesRequest): Promise<OfferedServiceTypes[]> {
    try {
      const offeredServiceType = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(OfferedServiceTypes, { where: offeredServiceType, relations: ['serviceType'] });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<OfferedServiceTypes> {
    try {
      const connection = await Connection.getConnection();
      const offeredServiceTypes = await connection.manager.findOne(OfferedServiceTypes, { id });
      return offeredServiceTypes
        ? offeredServiceTypes
        : Promise.reject(new NotFoundException(`Tipo de serviço prestado '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async createBatch(data: OfferedServiceTypesRequest[]): Promise<OfferedServiceTypes[]> {
    try {
      return Promise.all(data.map((data: OfferedServiceTypesRequest) => this.create(data)));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async create(data: OfferedServiceTypesRequest): Promise<OfferedServiceTypes> {
    try {
      OfferedServiceTypesController.validateFields(data);
      const connection = await Connection.getConnection();
      const offeredServiceType = await this.mapFromRequest(data);

      await connection.getRepository(OfferedServiceTypes).insert(offeredServiceType);
      return this.getOne(offeredServiceType.id);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async deleteAll(professional_email: string): Promise<void> {
    try {
      const connection = await Connection.getConnection();
      await connection
        .getRepository(OfferedServiceTypes)
        .delete({ professional: await new UsersController(this.user).getOne(professional_email) });
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async mapFromRequest(offeredServiceTypeRequest: OfferedServiceTypesRequest): Promise<OfferedServiceTypes> {
    const offeredServiceType = new OfferedServiceTypes();
    try {
      if (
        'service_type_id' in offeredServiceTypeRequest &&
        offeredServiceTypeRequest.service_type_id &&
        OfferedServiceTypesController.validateServiceTypeId(offeredServiceTypeRequest)
      )
        offeredServiceType.serviceType = await new ServiceTypesController(this.user).getOne(
          offeredServiceTypeRequest.service_type_id
        );
      if (
        'professional_email' in offeredServiceTypeRequest &&
        offeredServiceTypeRequest.professional_email &&
        UsersController.validateUserEmail({ email: offeredServiceTypeRequest.professional_email })
      )
        offeredServiceType.professional = await new UsersController(this.user).getOne(
          offeredServiceTypeRequest.professional_email
        );
      if (
        'base_value' in offeredServiceTypeRequest &&
        offeredServiceTypeRequest.base_value &&
        OfferedServiceTypesController.validateBaseValue(offeredServiceTypeRequest)
      )
        offeredServiceType.baseValue = offeredServiceTypeRequest.base_value;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
    return offeredServiceType;
  }

  static validateFields(data: OfferedServiceTypesRequest): void {
    if (!('service_type_id' in data)) throw new MissingParameterException(`Campo 'service_type_id' é obrigatório`);
    if (!('base_value' in data)) throw new MissingParameterException(`Campo 'base_value' é obrigatório`);
    if (!('professional_email' in data))
      throw new MissingParameterException(`Campo 'professional_email' é obrigatório`);
  }

  static validateServiceTypeId({ service_type_id }: { service_type_id?: number }): boolean {
    OfferedServiceTypesController.validateNumber(service_type_id as number, 'service_type_id');
    return true;
  }

  static validateBaseValue({ base_value }: { base_value?: number }): boolean {
    OfferedServiceTypesController.validateNumber(base_value as number, 'base_value');
    return true;
  }
}
