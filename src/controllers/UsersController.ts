// Libs
import moment from 'moment-timezone';
// Response types
import InvalidParameterValueException from '../response-types/4XX/InvalidParameterValueException';
import MissingParameterException from '../response-types/4XX/MissingParameterException';
import NotFoundException from '../response-types/4XX/NotFoundException';
import InternalErrorException from '../response-types/5XX/InternalErrorException';
import OperationNotPermittedException from '../response-types/4XX/OperationNotPermittedException';
import BadRequestException from '../response-types/4XX/BadRequestException';
// Request types
import UserRequest from '../request-types/UserRequest';
import AddressRequest from '../request-types/AddressRequest';
import OfferedServiceTypesRequest from '../request-types/OfferedServiceTypeRequest';
import AcceptedPaymentMethodRequest from '../request-types/AcceptedPaymentMethodRequest';
import ServiceRequest from '../request-types/ServiceRequest';
// Models
import Connection from '../database';
import Users from '../database/models/Users';
import Addresses from '../database/models/Addresses';
import OfferedServiceTypes from '../database/models/OfferedServiceTypes';
import AcceptedPaymentMethods from '../database/models/AcceptedPaymentMethods';
import Services from '../database/models/Services';
import Messages from '../database/models/Messages';
// Controllers
import Controller from './Controller';
import UserTypesController from './UserTypesController';
import AddressesController from './AddressesController';
import OfferedServiceTypesController from './OfferedServiceTypesController';
import AcceptedPaymentMethodsController from './AcceptedPaymentMethodsController';
import ServicesController from './ServicesController';
import MessagesController from './MessagesController';

export default class UsersController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(filters: UserRequest): Promise<Users[]> {
    try {
      const user = await this.mapFromRequest(filters);
      const connection = await Connection.getConnection();
      return connection.manager.find(Users, user);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(email: string): Promise<Users> {
    try {
      const connection = await Connection.getConnection();
      const user = await connection.getRepository(Users).findOne(
        {
          email
        },
        {
          relations: ['userType', 'userType.permissions', 'userType.permissions.resource']
        }
      );
      return user ? user : Promise.reject(new NotFoundException(`Usuário '${email}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(new InternalErrorException('Erro interno'));
    }
  }

  async create(data: UserRequest): Promise<Users> {
    try {
      UsersController.validateFields(data);

      const connection = await Connection.getConnection();
      const user = await connection.getRepository(Users).findOne({
        email: data.email
      });
      if (user) throw new BadRequestException('Este e-mail já está cadastrado');
      const userData = await this.mapFromRequest(data);

      await connection.getRepository(Users).insert(userData);
      return this.getOne(userData.email);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async update(email: string, data: UserRequest): Promise<Users> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');
      await this.getOne(email);
      const updatedUser = await this.mapFromRequest(data);
      const connection = await Connection.getConnection();
      await connection.getRepository(Users).update({ email }, updatedUser);
      return this.getOne(email);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(email: string): Promise<Users> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');
      const oldUser = await this.getOne(email);
      const connection = await Connection.getConnection();
      await connection.getRepository(Users).delete({ email });
      return oldUser;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getAddresses(email: string): Promise<Addresses[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      return new AddressesController(this.user).get({ user_email: email });
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getAddress(email: string, addressId: number): Promise<Addresses> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const addresses = await new AddressesController(this.user).get({ user_email: email, id: addressId });

      if (addresses.length !== 0) return addresses[0];
      else throw new NotFoundException(`Usuário '${email}' não possui o endereço '${addressId}'`);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateAddress(email: string, addressId: number, data: AddressRequest): Promise<Addresses> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const addressesController = new AddressesController(this.user);
      const addresses = await addressesController.get({ user_email: email, id: addressId });

      if (addresses.length !== 0) {
        return addressesController.update(addressId, data);
      } else throw new NotFoundException(`Usuário '${email}' não possui o endereço '${addressId}'`);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteAddress(email: string, addressId: number): Promise<Addresses> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const addressesController = new AddressesController(this.user);
      const addresses = await addressesController.get({ user_email: email, id: addressId });

      if (addresses.length !== 0) {
        return addressesController.delete(addressId);
      } else throw new NotFoundException(`Usuário '${email}' não possui o endereço '${addressId}'`);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async setOfferedServices(email: string, data: OfferedServiceTypesRequest[]): Promise<OfferedServiceTypes[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const offeredServiceTypesController = new OfferedServiceTypesController(this.user);
      await offeredServiceTypesController.deleteAll(this.user.email);
      return offeredServiceTypesController.createBatch(
        data.map((request: OfferedServiceTypesRequest) => ({ ...request, professional_email: email }))
      );
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOfferedServices(email: string): Promise<OfferedServiceTypes[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      return new OfferedServiceTypesController(this.user).get({ professional_email: email });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async setPaymentMethods(email: string, data: AcceptedPaymentMethodRequest[]): Promise<AcceptedPaymentMethods[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const acceptedPaymentMethodsController = new AcceptedPaymentMethodsController(this.user);
      await acceptedPaymentMethodsController.deleteAll(this.user.email);
      return acceptedPaymentMethodsController.createBatch(
        data.map((request: AcceptedPaymentMethodRequest) => ({ ...request, professional_email: email }))
      );
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getPaymentMethods(email: string): Promise<AcceptedPaymentMethods[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      return new AcceptedPaymentMethodsController(this.user).get({ professional_email: email });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getServices(email: string): Promise<Services[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      return new ServicesController(this.user).get({ user_email: email });
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getService(email: string, serviceId: string): Promise<Services> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const services = await new ServicesController(this.user).get({ user_email: email, id: serviceId });

      if (services.length !== 0) return services[0];
      else throw new NotFoundException(`Usuário '${email}' não possui o serviço '${serviceId}'`);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateService(email: string, serviceId: string, data: ServiceRequest): Promise<Services> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const serviceController = new ServicesController(this.user);
      const services = await serviceController.get({ user_email: email, id: serviceId });

      if (services.length !== 0) {
        return serviceController.update(serviceId, data, false);
      } else throw new NotFoundException(`Usuário '${email}' não possui o serviço '${serviceId}'`);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteService(email: string, serviceId: string): Promise<Services> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const serviceController = new ServicesController(this.user);
      const services = await serviceController.get({ user_email: email, id: serviceId });

      if (services.length !== 0) {
        return serviceController.delete(serviceId);
      } else throw new NotFoundException(`Usuário '${email}' não possui o serviço '${serviceId}'`);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getServiceMessages(email: string, serviceId: string): Promise<Messages[]> {
    try {
      if (this.user.email !== email) throw new OperationNotPermittedException('Permissão negada');

      const service = await new ServicesController(this.user).getOne(serviceId);

      if (service.user.email !== email && service.professional.email !== email)
        throw new NotFoundException(`Usuário '${email}' não possui o serviço '${serviceId}'`);

      return new MessagesController(this.user).get({ service_id: service.id });
    } catch (error) {
      return Promise.reject(error);
    }
  }

  static mapToRequest(user: Users): UserRequest {
    const userRequest: UserRequest = {
      name: user.name,
      surname: user.surname,
      birthdate: user.birthdate,
      email: user.email,
      last_access: user.lastAccess
    };
    if (user.userType) userRequest.user_type_id = user.userType.id;

    return userRequest;
  }

  async mapFromRequest(userRequest: UserRequest): Promise<Users> {
    try {
      const user = new Users();
      if ('name' in userRequest && userRequest.name && UsersController.validateName(userRequest))
        user.name = userRequest.name;
      if ('surname' in userRequest && userRequest.surname && UsersController.validateSurname(userRequest))
        user.surname = userRequest.surname;
      if ('email' in userRequest && userRequest.email && UsersController.validateUserEmail(userRequest))
        user.email = userRequest.email;
      if ('password' in userRequest && userRequest.password && UsersController.validatePassword(userRequest))
        user.password = userRequest.password;
      if ('birthdate' in userRequest && userRequest.birthdate && UsersController.validateBirthdate(userRequest))
        user.birthdate = userRequest.birthdate;
      if ('last_access' in userRequest && userRequest.last_access) user.lastAccess = userRequest.last_access;
      if ('user_type_id' in userRequest && userRequest.user_type_id && UsersController.validateUserTypeId(userRequest))
        user.userType = await new UserTypesController(this.user).getOne(userRequest.user_type_id);

      return user;
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static generatePassword(): string {
    let password = '';
    const rules = ['ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz', '0123456789', '@$!%*?&'];
    for (let i = 0; i < 32; i++) {
      const ruleIndex = Math.floor(Math.random() * rules.length);
      const rule = rules[ruleIndex];
      const ruleLength = rule.length;
      password += rule.charAt(Math.floor(Math.random() * ruleLength));
    }
    console.log(password);
    return password;
  }

  static validateFields(data: UserRequest): void {
    if (!('name' in data)) throw new MissingParameterException(`Campo 'name' é obrigatório`);
    if (!('surname' in data)) throw new MissingParameterException(`Campo 'surname' é obrigatório`);
    if (!('email' in data)) throw new MissingParameterException(`Campo 'email' é obrigatório`);
    if (!('password' in data)) throw new MissingParameterException(`Campo 'password' é obrigatório`);
    if (!('birthdate' in data)) throw new MissingParameterException(`Campo 'birthdate' é obrigatório`);
    if (!('user_type_id' in data)) throw new MissingParameterException(`Campo 'user_type_id' é obrigatório`);
  }

  static validateUserEmail({ email }: { email?: string }): boolean {
    UsersController.validateString(email as string, 'email');
    UsersController.validateEmail(email as string);
    return true;
  }

  static validateName({ name }: { name?: string }): boolean {
    UsersController.validateString(name as string, 'name');
    UsersController.validateEmptyString(name as string, 'name');
    return true;
  }

  static validateSurname({ surname }: { surname?: string }): boolean {
    UsersController.validateString(surname as string, 'surname');
    UsersController.validateEmptyString(surname as string, 'surname');
    return true;
  }

  static validateBirthdate({ birthdate }: { birthdate?: string }): boolean {
    UsersController.validateString(birthdate as string, 'birthdate');
    UsersController.validateEmptyString(birthdate as string, 'birthdate');
    UsersController.validateDate(birthdate as string);
    const today = moment().tz('America/Sao_Paulo');
    const inputDate = moment(birthdate).tz('America/Sao_Paulo');
    if (today.diff(inputDate, 'years') < 18)
      throw new InvalidParameterValueException('Usuário deve ser maior de idade (+18)');
    return true;
  }

  static validatePassword({ password, confirm_password }: { password?: string; confirm_password?: string }): boolean {
    UsersController.validateString(password as string, 'password');
    UsersController.validateEmptyString(password as string, 'password');
    if (
      !(
        /[a-z]/.test(password as string) &&
        /[A-Z]/.test(password as string) &&
        /[\d]/.test(password as string) &&
        /[@$!%*?&]/.test(password as string) &&
        /.{8,}/.test(password as string)
      )
    )
      throw new InvalidParameterValueException(
        'A senha deve conter letras maiúsculas, minúsculas, números, caracteres especiais (@$!%*?&) e no mínimo oito caracteres'
      );

    if (password !== confirm_password) throw new InvalidParameterValueException('As senhas não conferem');
    return true;
  }

  static validateUserTypeId({ user_type_id }: { user_type_id?: number }): boolean {
    UsersController.validateNumber(user_type_id as number, 'user_type_id');
    return true;
  }
}
