import Controller from './Controller';
import AccessLevelController from './AccessLevelController';
import { In } from 'typeorm';
import Connection from '../database';
import AccessLevel from '../database/models/AccessLevel';
import UserTypes from '../database/models/UserTypes';
import NotFoundException from '../response-types/4XX/NotFoundException';
import InvalidParameterValueException from '../response-types/4XX/InvalidParameterValueException';
import Users from '../database/models/Users';

export default class UserTypesController extends Controller {
  constructor(user: Users) {
    super(user);
  }

  async get(): Promise<UserTypes[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(UserTypes, { where: { id: In([2, 3]) }, order: { name: 'ASC' } });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getOne(id: number): Promise<UserTypes> {
    try {
      const connection = await Connection.getConnection();
      const userType = await connection.manager.findOne(UserTypes, { id });
      return userType ? userType : Promise.reject(new NotFoundException(`Tipo de usuário '${id}' não encontrado`));
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getPermissions(): Promise<UserTypes[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(UserTypes, { relations: ['permissions', 'permissions.resource'] });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async getUserTypePermissions(): Promise<AccessLevel[]> {
    try {
      const connection = await Connection.getConnection();
      return connection.manager.find(AccessLevel, { where: { userType: this.user.userType }, relations: ['resource'] });
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async addPermissions(id: number, body: { resources_ids: number[] }): Promise<AccessLevel[]> {
    try {
      UserTypesController.validateResourcesIds(body);
      return Promise.all(
        body.resources_ids.map((resourceId: number) =>
          new AccessLevelController(this.user).create({ user_type_id: id, resource_id: resourceId })
        )
      );
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  async deletePermission(id: number, resourceId: number): Promise<AccessLevel> {
    try {
      const accessLevelController = new AccessLevelController(this.user);
      const accessLevels = await accessLevelController.get({ user_type_id: id, resource_id: resourceId });

      if (accessLevels.length !== 0) {
        return accessLevelController.delete(accessLevels[0].id);
      } else throw new NotFoundException(`Tipo de usuário '${id}' não possui o recurso '${resourceId}'`);
    } catch (error: unknown) {
      return Promise.reject(error);
    }
  }

  static validateResourcesIds({ resources_ids }: { resources_ids: number[] }): boolean {
    UserTypesController.validateArray(resources_ids, 'resources_ids');
    UserTypesController.validateEmptyArray(resources_ids, 'resources_ids');
    resources_ids.forEach((id: number) => {
      if (typeof id !== 'number')
        throw new InvalidParameterValueException(`Campo 'resources_ids' deve ser um conjunto de inteiros`);
    });

    return true;
  }
}
