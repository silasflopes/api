// Libs
import moment from 'moment-timezone';

// Exceptions
import InvalidParameterTypeException from '../response-types/4XX/InvalidParameterTypeException';
import MissingValueException from '../response-types/4XX/MissingValueException';
import InvalidParameterValueException from '../response-types/4XX/InvalidParameterValueException';
import Users from '../database/models/Users';

export default class Controller {
  protected user: Users;

  constructor(user: Users) {
    this.user = user;
  }

  static validateObject(body: unknown, fieldName?: string): boolean {
    if (typeof body !== 'object' || Array.isArray(body) || !body) {
      const message =
        typeof fieldName === 'string'
          ? `Campo '${fieldName}' deve ser um JSON válido`
          : 'Campo deve ser um JSON válido';
      throw new InvalidParameterTypeException(message);
    }
    return true;
  }
  static validateString(property: string, fieldName?: string): boolean {
    if (typeof property !== 'string') {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' deve ser alfanumérico` : 'Campo deve ser alfanumérico';
      throw new InvalidParameterTypeException(message);
    }
    return true;
  }
  static validateArray(array: unknown[], fieldName?: string): boolean {
    if (!Array.isArray(array)) {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' deve ser um array` : 'Campo deve ser um array';
      throw new InvalidParameterTypeException(message);
    }
    return true;
  }
  static validateNumber(num: number, fieldName?: string): boolean {
    if (typeof num !== 'number') {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' deve ser numérico` : 'Campo deve ser numérico';
      throw new InvalidParameterTypeException(message);
    }
    return true;
  }
  static validateBoolean(bool: boolean, fieldName?: string): boolean {
    if (typeof bool !== 'boolean') {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' deve ser booleano` : 'Campo deve ser booleano';
      throw new InvalidParameterTypeException(message);
    }
    return true;
  }
  static validateEmptyObject(object: unknown, fieldName?: string): boolean {
    const message =
      typeof fieldName === 'string' ? `Campo '${fieldName}' deve ser um JSON válido` : 'Campo deve ser um JSON válido';
    if (typeof object !== 'object' || !object) throw new InvalidParameterTypeException(message);
    if (Object.keys(object).length === 0) {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' não pode estar vazio` : 'Campo não pode estar vazio';
      throw new MissingValueException(message);
    }
    return true;
  }
  static validateEmptyString(property: string, fieldName?: string): boolean {
    if (property.length === 0) {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' não pode estar vazio` : 'Campo não pode estar vazio';
      throw new MissingValueException(message);
    }
    return true;
  }
  static validateEmptyArray(array: unknown[], fieldName?: string): boolean {
    if (array.length === 0) {
      const message =
        typeof fieldName === 'string' ? `Campo '${fieldName}' não pode estar vazio` : 'Campo não pode estar vazio';
      throw new MissingValueException(message);
    }
    return true;
  }
  static validateDate(date: string, dateFormat = 'YYYY-MM-DD'): boolean {
    if (!moment(date, dateFormat, true).isValid())
      throw new InvalidParameterValueException(`'${date}' não é uma data válida`);
    return true;
  }
  static validateDateTime(datetime: string, dateFormat = 'YYYY-MM-DD HH:mm:ss'): boolean {
    if (!moment(datetime, dateFormat, true).isValid())
      throw new InvalidParameterValueException(`'${datetime}' não é uma data válida`);
    return true;
  }
  static validateEmail(email: string): boolean {
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!mailformat.test(email)) throw new InvalidParameterValueException(`'${email}' não é um email válido`);
    return true;
  }
  static validateUrl(url: string): boolean {
    try {
      new URL(url);
    } catch (error: unknown) {
      throw new InvalidParameterValueException('A url enviada não é válida');
    }
    return true;
  }

  static async wait(ms: number): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }
}
