declare type ServiceTypeRequest = {
  name?: string;
  id?: number;
};

export default ServiceTypeRequest;
