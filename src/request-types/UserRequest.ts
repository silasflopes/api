declare type UserRequest = {
  email?: string;
  name?: string;
  surname?: string;
  birthdate?: string;
  user_type_id?: number;
  password?: string;
  last_access?: string;
  confirm_password?: string;
};

export default UserRequest;
