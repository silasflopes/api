declare type AcceptedPaymentMethodRequest = {
  payment_method_id?: number;
  professional_email?: string;
};

export default AcceptedPaymentMethodRequest;
