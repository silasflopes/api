declare type ResourceRequest = {
  name?: string;
  id?: number;
};

export default ResourceRequest;
