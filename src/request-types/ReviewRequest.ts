declare type ReviewRequest = {
  service_id?: string;
  user_email?: string;
  rate?: number;
  comment?: string;
};

export default ReviewRequest;
