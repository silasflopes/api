declare type AddressRequest = {
  name?: string;
  address?: string;
  number?: number;
  complement?: string;
  district?: string;
  city?: string;
  state?: string;
  country?: string;
  zipcode?: string;
  user_email?: string;
  id?: number;
};

export default AddressRequest;
