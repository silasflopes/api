declare type MessageRequest = {
  message?: string;
  from?: string;
  service_id?: string;
  id?: number;
};
export default MessageRequest;
