declare type AccessLevelRequest = {
  resource_id?: number;
  user_type_id: number;
};

export default AccessLevelRequest;
