import AddressRequest from './AddressRequest';

declare type ServiceAddressRequest = AddressRequest & { id?: number; service_id?: string; address_id?: number };
export default ServiceAddressRequest;
