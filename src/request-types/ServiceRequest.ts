import ServiceAddressRequest from './ServiceAddressRequest';

declare type ServiceRequest = {
  id?: string;
  service_type_id?: number;
  payment_method_id?: number;
  user_email?: string;
  professional_email?: string;
  description?: string;
  service_date?: string;
  base_value?: number;
  status_id?: number;
  value?: number;
  addresses?: ServiceAddressRequest[];
  professional_acceptance?: boolean;
  user_acceptance?: boolean;
};

export default ServiceRequest;
