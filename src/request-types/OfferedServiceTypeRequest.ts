declare type OfferedServiceTypeRequest = {
  service_type_id?: number;
  professional_email?: string;
  base_value?: number;
};

export default OfferedServiceTypeRequest;
