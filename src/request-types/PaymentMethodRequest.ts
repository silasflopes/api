declare type PaymentMethodRequest = {
  name?: string;
  id?: number;
};

export default PaymentMethodRequest;
