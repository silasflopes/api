declare type MonthlyIncome = {
  month: number;
  income: number;
  service_name: string;
};
export default MonthlyIncome;
