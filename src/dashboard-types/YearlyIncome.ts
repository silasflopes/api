declare type YearlyIncome = {
  income: number;
  service_name: string;
};
export default YearlyIncome;
