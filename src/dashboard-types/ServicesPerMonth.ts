declare type ServicePerMonth = {
  month: number;
  quantity: number;
  service_name: string;
};
export default ServicePerMonth;
