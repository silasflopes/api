declare type PaymentMethods = {
  quantity: number;
  payment_name: string;
};
export default PaymentMethods;
