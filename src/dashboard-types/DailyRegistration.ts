declare type DailyRegistration = {
  day: number;
  quantity: number;
  user_type: string;
};
export default DailyRegistration;
