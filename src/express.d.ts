import Users from './database/models/Users';

declare global {
  namespace Express {
    interface Request {
      user: Users;
    }
  }
}
