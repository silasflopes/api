import { RequestHandler } from 'express';
import guardFactory from 'express-jwt-permissions';

export default function (permission: string | string[]): RequestHandler {
  return guardFactory().check(permission);
}
