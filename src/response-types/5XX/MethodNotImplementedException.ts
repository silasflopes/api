import CustomResponse from '../CustomResponse';

export default class MethodNotImplementedException extends CustomResponse {
  constructor(message: string) {
    super(501, message);
  }
}
