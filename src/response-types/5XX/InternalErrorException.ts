import CustomResponse from '../CustomResponse';

export default class InternalErrorException extends CustomResponse {
  constructor(message: string) {
    super(500, message);
  }
}
