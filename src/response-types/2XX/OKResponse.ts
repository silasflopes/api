import SuccessResponse from './SuccessResponse';

export default class OKResponse extends SuccessResponse {
  constructor(message: string) {
    super(200, message);
  }
}
