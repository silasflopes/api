import SuccessResponse from './SuccessResponse';

export default class CreatedResponse extends SuccessResponse {
  constructor(message: string) {
    super(201, message);
  }
}
