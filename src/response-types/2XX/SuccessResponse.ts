import CustomResponse from '../CustomResponse';

export default class SuccessResponse extends CustomResponse {
  constructor(code: number, message: string) {
    super(code, message);
  }
}
