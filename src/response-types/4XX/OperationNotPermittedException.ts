import CustomResponse from '../CustomResponse';

export default class OperationNotPermittedException extends CustomResponse {
  constructor(message: string) {
    super(403, message);
  }
}
