import BadRequestException from './BadRequestException';

export default class MissingValueException extends BadRequestException {
  constructor(message: string) {
    super(message);
  }
}
