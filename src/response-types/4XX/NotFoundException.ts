import CustomResponse from '../CustomResponse';

export default class NotFoundException extends CustomResponse {
  constructor(message: string) {
    super(404, message);
  }
}
