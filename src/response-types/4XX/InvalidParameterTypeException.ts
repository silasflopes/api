import BadRequestException from './BadRequestException';

export default class InvalidParameterTypeException extends BadRequestException {
  constructor(message: string) {
    super(message);
  }
}
