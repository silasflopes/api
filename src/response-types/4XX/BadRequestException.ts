import CustomResponse from '../CustomResponse';

export default class BadRequestException extends CustomResponse {
  constructor(message: string) {
    super(400, message);
  }
}
