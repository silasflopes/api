import BadRequestException from './BadRequestException';

export default class InvalidParameterValueException extends BadRequestException {
  constructor(message: string) {
    super(message);
  }
}
