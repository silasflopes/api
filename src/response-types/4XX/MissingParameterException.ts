import BadRequestException from './BadRequestException';

export default class MissingParameterException extends BadRequestException {
  constructor(message: string) {
    super(message);
  }
}
