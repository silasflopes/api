import CustomResponse from '../CustomResponse';

export default class UnauthorizedException extends CustomResponse {
  constructor(message: string) {
    super(401, message);
  }
}
