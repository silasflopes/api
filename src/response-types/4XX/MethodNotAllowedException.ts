import CustomResponse from '../CustomResponse';

export default class MethodNotAllowedException extends CustomResponse {
  constructor(message: string) {
    super(405, message);
  }
}
