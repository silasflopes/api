export default abstract class CustomResponse {
  public message: string;
  public code: number;

  constructor(code: number, message: string) {
    this.code = code;
    this.message = message;
  }
}
