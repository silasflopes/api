import express, { Request, Response, NextFunction } from 'express';
import jwt from 'express-jwt';
import PermissionError from 'express-jwt-permissions/error';
import cors from 'cors';
import { route as createRoutes } from './routers';
import Connection from './database/index';
import CustomResponse from './response-types/CustomResponse';
import UnauthorizedException from './response-types/4XX/UnauthorizedException';
import OperationNotPermittedException from './response-types/4XX/OperationNotPermittedException';

const app = express();
app.use(express.json());
app.use(cors());

// Middleware de autenticação
app.use(async (request: Request, response: Response, next: NextFunction) => {
  // Conecta com o banco caso não esteja conectado
  await Connection.getConnection();
  next();
});
createRoutes(app);
// Middleware para tratamento de erros
app.use((error: CustomResponse | jwt.UnauthorizedError, request: Request, response: Response, next: NextFunction) => {
  if (error !== undefined) {
    if (!(error instanceof CustomResponse)) {
      if (error instanceof jwt.UnauthorizedError) error = new UnauthorizedException('Não autorizado');
      if (error instanceof PermissionError) error = new OperationNotPermittedException('Permissões insuficientes');
    }

    // Verifica se o header já foi enviado, para não tentar enviar novamente
    if (!response.headersSent) response.status(error.code || 500).send({ message: error.message || 'Erro interno' });
  }
});
export default app;
