import { ConnectionManager, Connection as DBConnection } from 'typeorm';

export default class Connection {
  private static connectionManager = new ConnectionManager();
  private static connection: DBConnection;

  private static async connect() {
    const connectionName = 'myAwesomeConnection';
    try {
      Connection.connectionManager.get(connectionName);
    } catch (error: unknown) {
      Connection.connection = Connection.connectionManager.create({
        name: connectionName,
        type: 'mysql',
        logging: process.env.DB_LOGGING === 'true',
        dateStrings: true,
        entities: [__dirname + '/models/*.{ts,js}'],
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT as string),
        database: process.env.DB_DATABASE,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD
      });
      await Connection.connection.connect();
    }
  }

  public static async getConnection(): Promise<DBConnection> {
    if (!Connection.connection || Connection.connection.isConnected) await Connection.connect();

    return Connection.connection;
  }

  public static async close(): Promise<void> {
    Connection.connectionManager.connections.length = 0;
    if (Connection.connection.isConnected) return Connection.connection.close();
  }
}
