import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Resources from './Resources';
import UserTypes from './UserTypes';

@Entity()
export default class AccessLevel {
  @PrimaryGeneratedColumn()
  public id!: number;
  @ManyToOne(() => Resources, (resource) => resource.id)
  @JoinColumn({ name: 'resource_id', referencedColumnName: 'id' })
  public resource!: Resources;
  @ManyToOne(() => UserTypes, (userType) => userType.id)
  @JoinColumn({ name: 'user_type_id', referencedColumnName: 'id' })
  public userType!: UserTypes;
}
