import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export default class Resources {
  @PrimaryGeneratedColumn()
  public id!: number;
  @Column()
  public name!: string;
  @Column()
  public type?: string;
}
