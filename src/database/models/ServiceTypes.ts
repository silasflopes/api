import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class ServiceTypes {
  @PrimaryGeneratedColumn()
  public id!: number;
  @Column()
  public name!: string;
  @Column()
  public code!: string;
  @Column({ type: 'tinyint', name: 'requires_multiple_addresses', default: 0 })
  public requiresMultipleAddresses!: number;
}
