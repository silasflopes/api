import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import PaymentMethods from './PaymentMethods';
import Reviews from './Reviews';
import ServiceAddresses from './ServiceAddresses';
import ServiceTypes from './ServiceTypes';
import Status from './Status';
import Users from './Users';

@Entity()
export default class Services {
  @PrimaryGeneratedColumn()
  public id!: string;
  @ManyToOne(() => ServiceTypes, (serviceType) => serviceType.id)
  @JoinColumn({ name: 'service_type_id', referencedColumnName: 'id' })
  public serviceType!: ServiceTypes;
  @ManyToOne(() => PaymentMethods, (paymentMethod) => paymentMethod.id)
  @JoinColumn({ name: 'payment_method_id', referencedColumnName: 'id' })
  public paymentMethod!: PaymentMethods;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'user_email', referencedColumnName: 'email' })
  public user!: Users;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'professional_email', referencedColumnName: 'email' })
  public professional!: Users;
  @Column('decimal')
  public value!: number;
  @Column()
  public description!: string;
  @CreateDateColumn({ name: 'created_at' })
  public createdAt!: string;
  @UpdateDateColumn({ name: 'modified_at' })
  public modifiedAt!: string;
  @ManyToOne(() => Status, (status) => status.id)
  @JoinColumn({ name: 'status_id', referencedColumnName: 'id' })
  public status!: Status;
  @Column({ name: 'service_date' })
  public serviceDate?: string;
  @Column({ name: 'finished_at' })
  public finishedAt?: string;
  @Column({ type: 'decimal', name: 'base_value' })
  public baseValue!: number;
  @OneToOne(() => Reviews, (review) => review.service)
  public review!: Reviews;
  @OneToMany(() => ServiceAddresses, (serviceAddress) => serviceAddress.service)
  public addresses!: ServiceAddresses[];
  @Column({ type: 'tinyint', name: 'professional_value_acceptance' })
  public professionalValueAcceptance?: boolean | null;
  @Column({ type: 'tinyint', name: 'user_value_acceptance' })
  public userValueAcceptance?: boolean | null;
}
