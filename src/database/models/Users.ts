// Libs
import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn
} from 'typeorm';
import crypto from 'crypto';
// Models
import UserTypes from './UserTypes';
import AcceptedPaymentMethods from './AcceptedPaymentMethods';
import OfferedServiceTypes from './OfferedServiceTypes';
import Services from './Services';

@Entity()
export default class Users {
  @PrimaryColumn()
  public email!: string;
  @Column()
  public name!: string;
  @Column()
  public surname!: string;
  @Column()
  public birthdate!: string;
  @CreateDateColumn({ name: 'created_at' })
  public createdAt!: string;
  @ManyToOne(() => UserTypes, (userType) => userType.id)
  @JoinColumn({ name: 'user_type_id', referencedColumnName: 'id' })
  public userType!: UserTypes;
  @Column({ name: 'last_access' })
  public lastAccess!: string;
  @Column()
  public password!: string;
  @OneToMany(() => AcceptedPaymentMethods, (acceptedPaymentMethods) => acceptedPaymentMethods.professional)
  public acceptedPaymentMethods!: AcceptedPaymentMethods[];
  @OneToMany(() => OfferedServiceTypes, (offeredServiceType) => offeredServiceType.professional)
  public offeredServiceTypes!: OfferedServiceTypes[];
  @OneToMany(() => Services, (services) => services.professional)
  public workedServices!: Services[];
  public rate!: number;

  @BeforeInsert()
  public beforeInsert(): void {
    this.password = crypto
      .createHash('sha256')
      .update((process.env.SALT as string) + this.password)
      .digest('hex');
  }

  @BeforeUpdate()
  public beforeUpdate(): void {
    if (this.password)
      this.password = crypto
        .createHash('sha256')
        .update((process.env.SALT as string) + this.password)
        .digest('hex');
  }

  @AfterLoad()
  public beforeLoad(): void {
    this.password = '';
    if (this.workedServices) {
      let sum = 0;
      let reviewCount = 0;
      this.workedServices.forEach(({ review }: Services) => {
        if (review) {
          sum += review.rate;
          reviewCount++;
        }
      });
      this.rate = reviewCount === 0 ? 0 : sum / reviewCount;
    } else this.rate = 0;
  }
}
