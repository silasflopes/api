import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import Services from './Services';
import Users from './Users';

@Entity()
export default class Reviews {
  @PrimaryColumn({ name: 'service_id' })
  public serviceId!: string;
  @ManyToOne(() => Services, (service) => service.id, { primary: true })
  @JoinColumn({ name: 'service_id', referencedColumnName: 'id' })
  public service!: Services;
  @Column()
  public rate!: number;
  @Column()
  public comment?: string;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'user_email', referencedColumnName: 'email' })
  public user!: Users;
  @CreateDateColumn({ name: 'created_at' })
  public createdAt!: string;
}
