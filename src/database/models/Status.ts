import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class Status {
  @PrimaryGeneratedColumn()
  public id!: number;
  @Column()
  public name!: string;
  @Column()
  public short!: string;
  @Column({ type: 'tinyint', default: 0, name: 'is_ending' })
  public isEnding!: number;
}
