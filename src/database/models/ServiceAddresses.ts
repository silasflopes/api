import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Addresses from './Addresses';
import Services from './Services';

@Entity()
export default class ServiceAddresses {
  @PrimaryGeneratedColumn()
  public id!: number;
  @ManyToOne(() => Services, (service) => service.id)
  @JoinColumn({ name: 'service_id', referencedColumnName: 'id' })
  public service!: Services;
  @ManyToOne(() => Addresses, (address) => address.id)
  @JoinColumn({ name: 'address_id', referencedColumnName: 'id' })
  public address!: Addresses;
  @Column()
  public name!: string;
  @Column({ name: 'address' })
  public addressName!: string;
  @Column()
  public city!: string;
  @Column()
  public state!: string;
  @Column()
  public country!: string;
  @Column()
  public zipcode!: string;
  @Column()
  public number!: number;
  @Column()
  public district!: string;
  @Column()
  public complement?: string;
}
