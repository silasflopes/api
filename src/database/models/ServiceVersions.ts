import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import PaymentMethods from './PaymentMethods';
import Services from './Services';
import Users from './Users';

@Entity()
export default class ServiceVersions {
  @PrimaryGeneratedColumn()
  public id!: number;
  @ManyToOne(() => PaymentMethods, (paymentMethod) => paymentMethod.id)
  @JoinColumn({ name: 'payment_method_id', referencedColumnName: 'id' })
  public paymentMethod!: PaymentMethods;
  @Column('decimal')
  public value!: number;
  @Column()
  public description!: string;
  @CreateDateColumn({ name: 'created_at' })
  public createdAt!: string;
  @UpdateDateColumn({ name: 'modified_at' })
  public modifiedAt!: string;
  @Column({ name: 'service_date' })
  public serviceDate!: string;
  @ManyToOne(() => Services, (service) => service.id)
  @JoinColumn({ name: 'service_id', referencedColumnName: 'id' })
  public service!: Services;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'made_by', referencedColumnName: 'email' })
  public madeBy?: Users;
}
