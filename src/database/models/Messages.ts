import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Services from './Services';
import Users from './Users';

@Entity()
export default class Messages {
  @PrimaryGeneratedColumn()
  public id!: number;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'from', referencedColumnName: 'email' })
  public from!: Users;
  @Column()
  public message!: string;
  @CreateDateColumn({ name: 'created_at' })
  public createdAt!: string;
  @ManyToOne(() => Services, (service) => service.id)
  @JoinColumn({ name: 'service_id', referencedColumnName: 'id' })
  public service!: Services;
}
