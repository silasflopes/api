import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Users from './Users';

@Entity()
export default class Addresses {
  @PrimaryGeneratedColumn()
  public id!: number;
  @Column()
  public name!: string;
  @Column()
  public address!: string;
  @Column()
  public city!: string;
  @Column()
  public state!: string;
  @Column()
  public country!: string;
  @Column()
  public zipcode!: string;
  @Column()
  public number!: number;
  @Column()
  public district!: string;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'user_email', referencedColumnName: 'email' })
  public user!: Users;
  @Column()
  public complement?: string;
}
