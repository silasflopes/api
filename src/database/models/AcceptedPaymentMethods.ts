import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import PaymentMethods from './PaymentMethods';
import Users from './Users';

@Entity()
export default class AcceptedPaymentMethods {
  @PrimaryGeneratedColumn()
  public id!: number;
  @ManyToOne(() => PaymentMethods, (paymentMethod) => paymentMethod.id)
  @JoinColumn({ name: 'payment_method_id', referencedColumnName: 'id' })
  public paymentMethod!: PaymentMethods;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'professional_email', referencedColumnName: 'email' })
  public professional!: Users;
}
