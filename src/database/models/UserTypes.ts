import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import AccessLevel from './AccessLevel';

@Entity()
export default class UserTypes {
  @PrimaryGeneratedColumn()
  public id!: number;
  @Column()
  public name!: string;
  @OneToMany(() => AccessLevel, (accessLevel) => accessLevel.userType)
  public permissions!: AccessLevel[];
}
