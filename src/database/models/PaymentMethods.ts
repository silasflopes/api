import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class PaymentMethods {
  @PrimaryGeneratedColumn()
  public id!: number;
  @Column()
  public name!: string;
}
