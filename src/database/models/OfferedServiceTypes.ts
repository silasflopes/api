import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import ServiceTypes from './ServiceTypes';
import Users from './Users';

@Entity()
export default class OfferedServiceTypes {
  @PrimaryGeneratedColumn()
  public id!: number;
  @ManyToOne(() => ServiceTypes, (serviceType) => serviceType.id)
  @JoinColumn({ name: 'service_type_id', referencedColumnName: 'id' })
  public serviceType!: ServiceTypes;
  @ManyToOne(() => Users, (user) => user.email)
  @JoinColumn({ name: 'professional_email', referencedColumnName: 'email' })
  public professional!: Users;
  @Column({ type: 'decimal', name: 'base_value' })
  public baseValue!: number;
}
